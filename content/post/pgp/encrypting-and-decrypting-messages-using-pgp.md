---
date: "2018-01-21T14:29:00+05:30"
title: "Encrypting and Decrypting messages using PGP"
draft: false
slug : /encrypting-and-decrypting-messages-using-pgp/
tags : ["PGP", "Privacy"]
description: "Sending Secure Messages using PGP!"
---
{{< fluid_imgs
  "pure-u-1-1|/img/pgp/Encrypting-is-important.jpg|Encrypting is Important!"
>}}

>All the posts of this series is available here - [PGP Series](/tags/pgp/)

If you haven't yet understood the working of PGP then read my [previous article](/pretty-good-explanation-to-pgp/). For the sake of this article I will be using an online Encryption/Decryption program. You can download applications for your System from this [link](https://www.openpgp.org/software/).
{{< fluid_imgs
  "pure-u-1-1|/img/pgp/Generating-New-Keys.jpeg|Generating New Keys!"
>}}

No! Don't do this mistake, I generated these keys just for the sake of this article.. You must chose more stronger encryption method. After [generating](/pretty-good-explanation-to-pgp/#generating-keys) the keys save them to a safe location where no one can access them.

I am using [this](https://sela.io/pgp/) website for testing out PGP, you can go for any but make sure to check that the data is not sent to cloud.
## Encrypting Messages
{{< fluid_imgs
  "pure-u-1-1|/img/pgp/Encrypting-pgp-message.jpeg|Encrypting Message using PGP" >}}

{{<highlight java>}}
-----BEGIN PGP PUBLIC KEY BLOCK-----
Version: Keybase OpenPGP v1.0.0

Some random text here......
fhiabfnjkgnaklngkjraebngoiaeng
vaufnanfkjamerv
ajbnfoa

-----END PGP PUBLIC KEY BLOCK-----
{{</highlight>}}
Your Public Key will look like the example above, for encrypting the message you need to provide your Public Key and type in your Message. After some time you should get an output, something like this..

{{<highlight java>}}
-----BEGIN PGP MESSAGE-----
Version: OpenPGP v2.0.8

wYwD2LzxPfXaf5IBA/0Z9NtNCEifz/QYWqIXmGehweEpjqPfJ/lb+cJznvnlDJRP
Psvv1Ttv14Ck3WGGfkiiWH0wFs8ZXLpkgEEgs1nJqdETxSVJddLaJsJpXJYjhCDl
OW/MIGbSOron7I0Vm5q+S7MehUfA1Fwv+qo8RYDKRyAyPDIKS04L2ilvB9oZvNKw
AQTRU/zKPKN/ldHAHMaU/PAbav5adN1IqTvAhiy7mQr6AEsqC4VOTJAPrEsfF3Qy
67ms7Wgtulvg1OHBZCAZIR1gOGbyjYOSGDtRih+4Wfm9lSPegYtOGlwNUTreT+Ws
JtzNkmRkBG5rYlzY6J/UM1PCeXVGDxr5FqTCkOaA59upg+3ItUvpc3BuC82txYKz
NSWfSBrEx2+G5AtMESDmXUwI3Q77SblIfmJzQF+w9Vc=
=0ZhI
-----END PGP MESSAGE-----
{{</highlight>}}
This is your encrypted message and is now ready to be decrypted using your Private Keys.. because you have encrypted it with your Public Key, so it can only be decrypted using your Private Key.
### Decrypting Messages
{{< fluid_imgs
  "pure-u-1-1|/img/pgp/Decrypting-pgp-message.jpeg|Decrypting Message using PGP" >}}

For decrypting your PGP message you will have to provide the application with your Private Key, the Encrypted Message and the Passphrase to decrypt your Private Key.

> Your Private Key is encrypted with the Passphrase and that is used to decrypt it!

{{<highlight java>}}
-----BEGIN PGP PRIVATE KEY BLOCK-----
Version: Keybase OpenPGP v1.0.0

Some Random Text over here...

uhuafioaer;jgpiaejrpigerag
reagfhaoewigjaoerjgpaor
hfoiaehgpia

-----END PGP PRIVATE KEY BLOCK-----

{{</highlight>}}

This is how your Private Key will look like, once you click submit the message will be decrypted within some seconds. Decrypting may take some time if you are running an older system or the message is too large!

>All the posts of this series is available here - [PGP Series](/tags/pgp/)
