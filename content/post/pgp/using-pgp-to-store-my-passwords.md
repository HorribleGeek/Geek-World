---
date: "2018-01-26T14:29:00+05:30"
title: "Using PGP to encrypt Passwords - PGP in Daily Life"
draft: false
slug : /using-pgp-to-encrypt-passwords/
comments: ["Isn't this a great idea? - HorribleGeek"]
tags : ["PGP", "Privacy"]
description: "Using PGP in my Daily Life to manage all my Passwords!"
---
{{< fluid_imgs
  "pure-u-1-1|/img/pgp/pgp-to-encrypt-password.jpg|Using PGP to encrypt Passswords!"
>}}

>All the posts of this series is available here - [PGP Series](/tags/pgp/)

I have already explained the [working of PGP](/pretty-good-explanation-to-pgp/), using _PGP to encrypt your Passwords_ is probably the best method to encrypt it because it will be impossible to break it... if you don't mess it up!

> Password management should be simple and follow Unix philosophy. With pass, each password lives inside of a gpg encrypted file whose filename is the title of the website or resource that requires the password.

[Here](https://www.passwordstore.org) you will find the list of all the clients, there is one available for all the major platform. I sync this in my _GitLab Repository_, it is kept _private_ but there is actually no need to keep it private.. because all other users will see is this..
{{<highlight go>}}-----BEGIN PGP MESSAGE-----

hQIMA4w8G5zDo/4CAQ//QzVXMzOe+cn+RfT82OQ0SJ/blQM+DAbKuolJi5DrAlQ1
tMZItOZtnavt/uN9GY33gTLAHQhCGBdRVoH+2VucL5N0ooFM4kFli3kwEh0qA3ep
H1eb8xKMXT/FOgIgO8nYcd5RXQYR7XwkwdSNfFIcGb1gPcr9AgC8EtAfyVbnq/YR
5zOZxspOlwDSX8D6Mhdg0XTU5Y2TQWNgGCvJabGtAomCJbEPsks9kmCbdKS9Xd9h
kPZ5VRDM/DhRfqzIel0e3115k2uVwkomN/tL/my5Rgo5H/ZZ2rBFr2C289dDSPLr

This is very long.. I shortened it to 1/10th of its original size..

-----END PGP MESSAGE-----
{{</highlight>}}
This is the actual text stored in my _GitLab Repository_, this is the password for my _GitHub account_. Hack it if you can ;)

I mainly use my Android Device, there is a great App for Android called __Password Store__ (F-Droid). When you enter your Password in any of the clients mentioned above, your Password is __encrypted__ using your __PGP Public Key__.

If you lose your Android device then you can always get your _Passwords_ back by logging into your _Git Account_ (or any other cloud) and decrypting them manually using your __PGP Private Key__.

Using this method to store your Passwords, even on _Public Repository_ is completely safe unless you mess it up! I would recommend using a _Private Repository_ if you can, just in case if we imagine offline _brute-forcing_.. which is very unlikely to break a 4096 bit encryption.

> Keep your Private Key safe!

Previously, I wrote an article on using [Master Password](/is-your-password-manager-really-secure/). That is also a great option but if you need to store multiple username or anything like special note, then go for this!

>All the posts of this series is available here - [PGP Series](/tags/pgp/)
