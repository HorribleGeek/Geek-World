---
date: "2018-01-21T01:53:50+05:30"
title: "A Pretty good explanation to Pretty Good Privacy (PGP)"
draft: false
slug : /pretty-good-explanation-to-pgp/
tags : ["PGP", "Privacy"]
description: "PGP Explained!"

---
{{< fluid_imgs
  "pure-u-1-1|/img/pgp/Privacy-concerns.png|Using PGP is Great!"
>}}

>All the posts of this series is available here - [PGP Series](/tags/pgp/)

If you're like me, concerned about your privacy.. then this is for you. I am going to write a series of articles on this topic, we will start by learning about the working of _PGP_ then move on to encrypting messages to encrypting files and then using it in your daily life.

PGP stands for _Pretty Good Privacy_ and is mostly used by everyone to encrypt their messages. I use it for encrypting my messages, files, passwords.. etc,.
### Working of PGP
First it is important to learn about the working of PGP, I'll try to explain this to you. Imagine a scenario...

> You and your friend want to talk something secret, so he sends you a letter. That bad letterman reads your letter.. I don't know how.. but still keep imagining. Now this time you try to be smart and keep the letter in a box and lock it!

> Then you think... how will your friend open the box without the key...? So you send the key with the box! Again that bad guy reads your letter..! Imagine!

> This time you are totally fed-up, so you trigger your stupid mind and get an idea! You quickly make a lock with two keys... Confusing..? Don't worry things will get clear..

> You make a lock with two keys, one key to open the lock and one key to lock it! You call the key to lock the box as __Public Key__.. and the key to open the box as __Private Key__...

> It is called _Public Key_ because you can share it with anyone.. they'll lock the box with the message and send it to you freely. No other key except _your Private Key_ can open the box!

> You tell your friend to do the same and both of you exchange your Public Keys! Now when you have to send a message to your friend, you lock it in the box with his Public Key.. Remember..? Only his Private Key will be able to open it!

> No one issue is still there, now this time the letterman changes your message... He is also having your Public Key, so he encrypts a new message using your Public Key and swaps it with your original message.

> To counter this, we sign the messages. Signing can be done wit Private Keys only and it confirms from where the message has arrived.

That's how PGP works, while generating keys you generate two Keys - Public Key and Private Key. Both of them are _linked_ with some fancy math, which we are not going to discuss. Your Private Key once lost is gone forever, so it is to be kept _safe and private_.

### Terms Related to PGP
- Private Key : Also called as secret key and is meant to be kept _secret_.
- Public Key : Distribute it to everyone!
- Passphrase : Used to decrypt Private Key! Without Passphrase your Private Key is nothing!
- E-Mail : Related with your Keys, if you use E-Mail to generate keys then everytime you share your Public Key, you share your E-Mail with it!
- PGP Key Provider : Application which provides other applications with your PGP Key on request.
- Expiry : Keys Expiry time! Can be changed anytime by the owner.

#### Passphrase
It is used to encrypt your Private Key, you can change it later. That will not change your Private Key.. it will just decrypt it, then encrypt it with new Passphrase.
<center>
{{< fluid_imgs
  "pure-u-1-1|/img/pgp/Pgp-Explained.png|PGP Explained!"
>}}
</center>

This is a good explanation of working of PGP. If you don't understand this then move on, don't get confused with the diagram.
### Generating Keys
So now that you've understood about the working of PGP you can move on to generating the keys. While there are many applications both online and offline to generate your keys but I'll mention the most simple one.

I myself used this generator to generate my keys, it is called [pgpkeygen](https://pgpkeygen.com). There are many other websites which do the same but this one uses latest and most modern methods. If you generate with other generators and then import it in your PGP Key Provider then it will show error.. not signed.
<center>
{{< fluid_imgs
  "pure-u-1-1|/img/pgp/Generating-New-Keys.jpeg|Generating New Keys!"
>}}
</center>

No! Don't do this mistake, I generated these keys just for the sake of this article.. You must chose more stronger encryption method.

Remember that this thing should be completely offline, if you are going for some other website or application then be careful. This website doesn't send any data to the cloud after generating your keys.

> I am not promoting this website in any manner, also I am not associated with this Website!

This asks for basic details, fill all of them. For algorithm select RSA and key size can be anything of your choice. 1024 bits is not at all recommended, go for _4096 bits_ if you are running on a good system. I was trying to generate 4096 bits key on my Mobile Device and that took around 10 minutes!

Choose Expiry time as 1 year, it can be changed anytime. 1 year is recommended because if you lose your Keys at any point of time then you will not be able to revoke them and it will stay online with No Expiry!

> Once you generate your Keys, you can upload them to most popular Key Base where your Public Keys will be stored online for anyone to access!

I will be publishing more articles on this topic, we'll learn more about PGP, encrypting messages, decrypting them, safe practices and then using PGP in our daily lives!

>All the posts of this series is available here - [PGP Series](/tags/pgp/)
