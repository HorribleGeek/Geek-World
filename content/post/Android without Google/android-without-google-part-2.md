---
title: "Android Without Google Part 2 - Alternatives to Google Drive, Mail, Hangouts/Allo/Duo"
date: 2017-12-23T23:57:02+05:30
description: "In this article you'll find alternatives to Google Drive, GMail (both Client and Service), Hanouts/Allo/Duo!"
draft: false
slug : /android-without-google-part-2/
tags : ["Android-without-Google", "Privacy"]
aliases:
       - /2017/12/android-without-google-part-2.html
       - /2017/12/how-to-use-android-without-google-part-2.html
       - /how-to-use-android-without-google-part-2
---
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/android-without-google-2-1.jpeg|Android without Google"
  >}}

>All the posts of this series is available here - [Android without Google!](/tags/android-without-google/)

## GMail - The worst service you use!
>Click [here](#e-mail-client-to-use-for-android) for Client Alternative.

Did you know that Google _scans all your mails_ for Spam? Well, that is okay but it goes further more. It _scans your mails_ to build your __advertising profile__. Yes, all your mails is being _scanned by bots_ and they build your advertising profile out of that. Now, this is a serious issue. Why will we want someone to read our mails? Whether it is a bot or a human.

All providers that you will find here operate outside US and use _SSL_. Still, never trust your provider always _encrypt_ it yourself.
### [Proton Mail](https://www.protonmail.ch/)
Probably you would have heard of this service, if you were already looking for __Private__ E-Mail Provider. They rank the highest for search results like _encrypted email_ and _private email provider_.

It is since 2013, they also have an [onion](https://protonirockerxow.onion/) url. Their service is hosted in _Switzerland_ and provide you with _500 MB_ of Mailbox for free. They also have a _Premium Plan_ and a VPN Service which we will discuss later. This is the best you can get for free!
### [Tutanota](https://www.tutanota.com/)
This is the second service which I'd recommend you to chose. It is up and running since 2011. Their service is hosted in _Germany_ and provide you with _1GB_ of Free Mailbox.

They have a pretty solid encryption and assure you that your mails will be _private_, even to someone who doesn't use __Tutanota__. If you mail some other user then you'll have to decide a password and that user will have to type that in order to read the Mail.

They are serious about your _Privacy_!
### [Disroot](https://disroot.org/)
If you're using google then you are going to miss its ecosystem. Disroot does the same, you get _Cloud Service_, _Email_ and many other services for _free_.

_Disroot_ is a project based in _Amsterdam_, it is _maintained by volunteers_ and depends on the support of its community. They are since 2015 and their service is hosted in __Netherlands__ and provide you with 4GB of storage which is synced with Drive and all other Services.

The best is that they are powered by __NextCloud__, so you can trust them. It's completely _open-source_.
### [StartMail](https://www.startmail.com/en/)
Remember we talked about _Startpage_? StartMail is a Mail service provided by their founders.Here's what their CEO, Robert Beens has to say about StartMail:
>StartMail is built by the people behind StartPage and Ixquick, the world’s most private search engines. As early as 2005 we recognized privacy as a fundamental human right. We turned out to be ahead of our time. Over the next decade, revelation after revelation showed how much our online privacy had come under attack.

>In response, we built more and more defenses into our search engines to protect our users. People now use StartPage and Ixquick to find information millions of times per day, without being tracked or profiled.

They are based in _Netherlands and Europe_ where _Privacy is really strict_, so you can trust them. However it is a _paid service_ starting from $59.95 for 10GB of Space for a year. You can also sign up for 7 Day free trail!
> Prices as of January 2018, changed..? Update it or comment below!

## E-Mail Client to use for Android
Leaving GMail? Using their client will be more foolish thing someone can do! _Proton Mail_ and _Tutanota_ have their own _E-mail Client_ that you'll have to use because their data in encrypted right before going to the servers.
### [K-9 Mail](https://github.com/k9mail/k-9/releases)
Unfortunately you have no other option than this. Thats what I could search for, if you have any other _open source_ and free alternative then do share below in comments!

Their UI is pretty old, they also have a _dark theme_ but thats not for what you use _E-Mail client_. You can set up aliases and also use _OpenPGP encryption_.
>_Edit:_ The Logo has been updated, here you see the old logo!

If you're on desktop then you have several other options like __Mozilla Thunderbird__ and __Claws Mail__.
## Hangouts/Allo/Duo Alternative
If you are the old school guy that uses _Hangouts or Allo or Duo_ then you need to __switch instantly__. Now, _WhatsApp_ is no better, remember it's owned by _Facebook_! For Sure you are going to miss Google Assistant, can't do anything about that. Here are two alternatives that I recommend, you are also going to miss the UI.
### [Signal](https://signal.org/)
Signal is a mobile app developed by _Open Whisper Systems_. The app provides _instant messaging_, as well as _voice_ and _video_ calling. All communications are __end-to-end encrypted__. Signal is __free and open source__, enabling anyone to verify its security by auditing the code. The development team is supported by _community donations and grants_. There are no advertisements, and it doesn't cost anything to use.
### [Wire](https://get.wire.com/)
Wire is an app developed by _Wire Swiss GmbH_. The Wire app allows users to exchange __end-to-end encrypted__ _instant messages_, as well as make _voice and video calls_. Wire is _free and open source_, enabling anyone to verify its security by auditing the code.

>I would recommend you to got for Signal, as it has better UI and developement is also getting better.

## Google Drive Alternative
Dropbox, Onedrive, iCloud nothing is better than Google Drive. However iCloud is a good option but not the best. Remember Disroot, that we talked about above? They also provide cloud service, for free.

>iCloud is not as bad as other services because Apple doesn't collect that much of user data. It is almost all anonymous collection to improve their service.

### [Disroot](https://disroot.org/)
I personally use _Disroot_ service because I needed an ecosystem like Google Provides. They are using _Next Cloud_, which is completely open source and free. You can trust anything that uses Next Cloud. You can also browse your files on your Android using the NextCloud application from _F-Droid_.
### [Seafile](http://seafile.com/)
Seafile offers 100 GB Storage for $10/month but also gives you the opportunity to host on your own server. Your data is stored in _Germany_ or with _Amazon Web Service_ in the US for the cloud version. They have an _Android Client_, although they assure you your Privacy, still I won't recommend going with this.
### [NextCloud](https://nextcloud.com/)
This is something I would have gone for, it is completely _open source_ and somewhat like Dropbox. This is only for Geeks, however you will find many Youtube tutorials on setting it up.

>All the posts of this series is available here - [Android without Google!](/tags/android-without-google/)

It has no limits on users or space. You'll have to setup your own server or buy hosting from another provider and install it there and then use it remotely from anywhere.
