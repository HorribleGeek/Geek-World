---
title: "[Guide] How to Use Android Without Google - Ditching Google!"
date: 2017-12-22T21:05:01+05:30
draft: false
slug : /how-to-use-android-without-google-part-1/
description: "After writing this article I realised that this is more than Android without Google! You will get alternatives to all the products over here but the applications are for Android and so the Name of this Article!"
tags : ["Android-without-Google", "Privacy"]
comments: ["I also use Searx, it's the best alternative. - Unknown", "Nothing can beat Google, at least not in the near future - Tad", "@Tad Yes this is true, many still don't realise the value of Privacy - HorribleGeek", "I should mention Firefox Klar, it's a browser from F-Droid. It's great for opening external links from Reddit, 'Slide for Reddit' app to be precise, also the best product available in F-Droid. - HeyThat'sPrettyGood", "@HeyThat'sPrettyGood Added it to the list! - HorribleGeek"]
aliases:
       - /2017/12/how-to-use-android-without-google.html
       - /2017/12/how-to-use-android-without-google-part-1.html
       - /how-to-use-android-without-google
---
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/android-without-google-1-1.jpeg|Android without Google"
  >}}

>This is a series of __7 posts__ to navigate simple replace the number in the _url_. Ex: ...-1 to ...-2 will take you to part 2.

>There will be a __warning__ written if there is something more that you should consider before using the specfic service.

>All the posts of this series is available here - [Android without Google!](/tags/android-without-google/)

Any Issues? Google it! It is a 70% Probability that you have landed here through a Google Search.
Google is everywhere, it controls our live. It knows more about you than you know about yourself. It can predict which link you're going to click next, what you're going to search next.

It is so convenient right? Yes, it is, but that's not worth your Privacy. Android's market is 86% and increasing every year. Almost ~80% of us have Android device and pretty much know that it comes with all the Google Applications Pre-Installed.

If you've read my previous posts then you know that I'm a bit serious about privacy. Whether its a bot or a human, I simply don't want anyone to read my mails, track me, make my advertising profile, sell me! After all __Privacy is Privacy__. You can disable most of the Applications, but still that doesn't cut you off from Google.
## Privacy? But why, I have nothing to hide!
>Over the last 16 months, as I've debated this issue around the world, every single time somebody has said to me, "I don't really worry about invasions of privacy because I don't have anything to hide." I always say the same thing to them. I get out a pen, I write down my email address. I say, "Here's my email address.

>What I want you to do when you get home is email me the passwords to all of your email accounts, not just the nice, respectable work one in your name, but all of them, because I want to be able to just troll through what it is you're doing online, read what I want to read and publish whatever I find interesting. After all, if you're not a bad person, if you're doing nothing wrong, you should have __nothing to hide__." Not a single person has taken me up on that offer.  

>    [ - Glenn Greenwald in Why privacy matters - TED Talk ](https://www.ted.com/talks/glenn_greenwald_why_privacy_matters)

In this post, I will share all the Best __Alternatives to Google__ & its __Products__. Most of them are going to be __Open Source__ and __Privacy Focused__.

> This blog is now open-source and is hosted on __Github__ Pages!

First things first, Why do you want to leave google? Hmm.. Just because you want your _Privacy back_ or simply you love _Open-Source_. Android itself is open-source but most of the Applications on Play Store, is not!

Going Google Free isn't a piece of cake because you are leaving a lot of features. You won't get to use _Google's Applications_, though there will be better alternatives for most of them.
## Google Play Store Alternative
You are really going to miss a lot of applications, but don't worry if you like some application then you can download the _.apk_ file for it from other _Websites_. Here we are talking about a solid App Store. The Best you can go for is _Amazon App Store_, you will not find all the applications but I'm pretty sure that you will get most of the popular applications.
### [Amazon App Store](https://en.wikipedia.org/wiki/Amazon_Appstore)
It has been since 2011 and has over __350,000 applications__. If you are the one who doesn't trust google but want to go with a popular name then this is for you. Frankly speaking, there will be no difference in _tracking_. Both these giants _track you_, amazon also _tracks_ the amount of time you spend with that app, and they pretty much _trade data_ too! So, that will be of no use if you're migrating for _Privacy Purpose_.
### [F-Droid](https://f-droid.org/)
Remember, talking about Privacy and Open-Source? __F-Droid__ is itself completely __Open Source__ and the applications are also open source. Most of them at least are open source and private, __F-Droid__ displays a warning over applications which aren't open-source or break your __Privacy__.

The new update also has _material design_ and a cool dark theme so that you don't feel outdated. I myself use F-Droid to download and update applications. All the Applications you get there are free and verified, so _no_ issues of _malwares_. Remember it is powered by your __Donations__!
### Other Alternative ways
If you worry about getting apk files which are modded and will harm your device, then head straight to __CNET__ or __Apk Mirror__. They are the most popular and trusted source.
>You should never trust these apk file hosts, if you do then make sure to check the intergrity of the file you have downloaded.

Still want Apps from Play Store..? Get __Yalp Store__ which is also open-source and they give you _apk_ directly from Play! _Yalp Store_ is available in _F-Droid_.
## Google Search Alternative
There are many rescue points over here. [DuckDuckGo](https://duckduckgo.com/), [StartPage](https://www.startpage.com/), [Qwant](https://www.qwant.com/), [Searx](https://searx.me/) are some of the search engines you might consider for ditching Google. I am not writing much about __privacy__ and other stuff because everything is Privacy Focused here. There is a small summary table below, _summarising everything_.
>This is in increasing order of choice, i.e, the best is the last!

### [Qwant](https://www.qwant.com/)
Speaking of _Qwant_, it has a pretty much _bulky home page_, which you can _customise_. There are no specific themes for searches as of 2017. _No Logs_, _Cookie-Free Search Engine_, this is a good alternative over Google. It only lacks in design and customisation, otherwise it is perfect!
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/qwant-google-alternative.png|Qwant Search Engine"  >}}

> __Warning:__ This is _not Open-Source_!

### [DuckDuckGo](https://duckduckgo.com/)
_DuckDuckGo_ is a real google competitor, everywhere you will find articles on _DuckDuckGo vs Google_. It is a great place for programmers and developers as it brings answers directly from _StackOverflow_. The Interface is __simple and clean__, like google.

You have a bunch of __themes__ available for your searches, you can also _turn off advertisements_ and some other settings.
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/duckduckgo-settings.png|DuckDuckGo Settings Page"  >}}

> __Warning:__ It is a __US based company__, which we must avoid. Some of the code is public and open source but the core is private. Moreover it uses Amazon Servers, which means all your data goes through _amazon servers_. Not pretty good choice!

### [StartPage](https://www.startpage.com/)
With a bunch of customisations, StartPage is the best you can get over here. It has a night theme and other quick settings. Also, comes with proxy settings which means that you can visit a website without actually going there. It's like __StartPage__ goes to the website and brings it to you, so the website just thinks that StarPage has visited and not you.

It has the most clear __Privacy Policy__, the founders also have a good background and run only __StartPage__, StartMail and Ixquick, which is another Search Engine.
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/Startpage-search.png|StartPage Search"  >}}

The unique thing about _StartPage is that it is not a Search Engine itself_. It brings the Google Search to you, without sharing any details to google. They keep no logs, but you get tailored searches that is pretty much unclear, it seems that it shares the country location to google and brings the result. This results in _slower query responses_ compared to DuckDuckGo.

The Best of all, it is _not a US or UK based service_, so the NSA (National Security Agency) (Intelligence Agency) has no control over it, where as DuckDuckGo is US based company.
### [Searx](https://searx.me/)
My Personal Favourite and the best of all, __Searx__! It crawls other Search Engines and brings the results to you, there are some themes available and a bunch of advance settings. You can select which search engine's results you want, there is Image Proxy, etc,. It took me sometime to get used to the _Search Engine_ but once you explore it, you won't leave it.

{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/searx-the-best-google-alternative.png|StartPage Search"  >}}

###  Summary
{{< pure_table
  "Search|Source|Useful for.."
  "StartPage|Open|If you want Google Search with no tracking"
  "DuckDuckGo|Partial|Super-Cool! Keeps No Logs, US Company.. You decide!"
  "Searx.me|Open|Best of All! Good Image Search and Customisable!"
>}}

## Browser - Chrome Alternatives
This also is not a big issue you'll face. Ultimately, you will find the browser that suits your needs.
### [Firefox](https://www.mozilla.org/en-US/firefox/)

> Firefox Focus (Play Store) or Klar (F-Droid) is another option.

My best bet will be __Firefox__, as it is the only option you have if you want _multi device sync_!

> Also see Waterfox, which is a Firefox Fork with telemetry removed.

Firefox is a fast, reliable and a open source browser that respects your Privacy. It has a pretty _good interface and design_. The _latest release_ of __Firefox Quantum__ has created a lot of fuss on twitter, reddit, everywhere... It claims to be __2x__ times faster than old release and __30% lighter on memory__ compared to chrome.
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/Firefox-Quantum.png|Firefox Quantum"  >}}
### [Brave](https://www.brave.com/)
The new _open source_ browser __"Brave"__ automatically blocks ads and trackers, making it faster and safer than your current browser. Brave is based on __Chromium__. It is new in the market, I myself use Brave because I'm not a fan of multi device sync.
### [Orfox](https://guardianproject.info/apps/orfox/)
Orfox, which is __Tor__ equivalent to Android. It uses _Orbot_ to route your traffic through Tor Servers. __Tor Browser__ is your choice if you need an extra layer of __anonymity__. It's a _modified version of Firefox_, it comes with pre-installed privacy add-ons, encryption and an advanced proxy. This is for those who want to do some serious stuff with their Browsers. Like visiting Dark-Web...

> All the posts of this series is available here - [Android without Google!](/tags/android-without-google/)

That's all folks, I will publish a new post on other _Google Alternatives_. Remember, I've done all these and now my Android is completely Google-Free, so everything you will find here will be secure. Subscribe to my Newsletter to get Notified when, I publish new posts!
