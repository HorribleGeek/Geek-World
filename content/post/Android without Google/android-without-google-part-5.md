---
title: "Android Without Google - Part 5 - Hardening your Security - Tor and VPN"
date: 2018-01-11T01:04:41+05:30
description: "After finding alternatives to most of the Services, we will look into other ways or Hardening your Security!"
draft: false
slug : /android-without-google-part-5/
tags : ["Android-without-Google", "Privacy"]
comments: ["TOR is really great service but many misuse it. - Sahil", "@Sahil I agree. - HorribleGeek"]
aliases:
       - /2017/12/android-without-google-part-5.html
       - /2017/12/how-to-use-android-without-google-part-5.html
       - /how-to-use-android-without-google-part-5
---
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/going-anonymous-tor-vpn.jpeg|Going Anonymous with TOR and VPN"
  >}}

>All the posts of this series is available here - [Android without Google!](/tags/android-without-google/)

## Tor for Android
That's all? Is it over? Are we completely secure now? No! using Android? You are never secure! We still have options to explore, have you heard something called __Tor__?
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/how-tor-works-1-1.png|TOR Explained!"
  >}}

This is the best explanation that I've found over internet. That is how Tor works, now can we route our Android traffic through tor? Yes it is possible to route all your android traffic through tor using this application called [__Orbot__](https://guardianproject.info/apps/orbot/). The recommended browser is [__Orfox__](https://guardianproject.info/apps/orfox/), however which their new feature called __Apps VPN mode__ you can use any browser to browse Onion links.

If you use _Apps VPN Mode_ then your selected apps traffic is routed through tor and you're anonymous to the world, at least if you don't mess up!

>Image Source: _NordVPN_

## VPN (Virtual Private Network)
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/how-does-a-vpn-work.jpeg|VPN Service Explained!"
  >}}
The above image will help you understand the working of a VPN, basically it is TOR with only one relay. You can first connect to VPN then to TOR for better Privacy! I've tried and tested many VPN services (trial period) most of them were paid and expensive but I found one gem from the list which was really worth your time.

Introducing.. [ProtonVPN](https://protonvpn.com/) from the founders of Proton Mail! Their servers are in countries like Iceland which value your privacy. Nothing to say about this, just go with it. You can check their website for more information, although they have premium plans they were the only one with really cool free package!

>Image Credits: _VPN Radar_

>All the posts of this series is available here - [Android without Google!](/tags/android-without-google/)

That's all for this part, I'll publish the next part soon, Subscribe to my Newsletter to stay updated with my posts.
