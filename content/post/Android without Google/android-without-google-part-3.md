---
title: "Android Without Google Part 3 - Alternatives to Google +, Keep, Youtube"
date: 2018-01-02T23:57:02+05:30
description: "In this article you'll find alternatives to GooglePlus, Keep, Youtube (both Client and Service)!"
draft: false
slug : /android-without-google-part-3/
tags : ["Android-without-Google", "Privacy"]
comments: ["You forgot to mention SkyTube.. - Chris", "@Chris Thank you for the suggestion, I'll try it! - HorribleGeek", "HookTube is also Interesting - Unknown", "@Unknown HookTube is good but it is somewhat like NSFW Youtube - HorribleGeek", "Omni Notes FOSS is tremendous achievement, available on F-Droid. It is similar to a certain extent to Evernote but the best thing about Omni Notes is ... that it's not Evernote - HeyThat'sPrettyGood", "@HeyThat'sPrettyGood Added OMNI Notes FOSS to the list! - HorribleGeek"]
aliases:
       - /2017/12/android-without-google-part-3.html
       - /2017/12/how-to-use-android-without-google-part-3.html
       - /how-to-use-android-without-google-part-3
---
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/android-without-google-3.jpeg|Android without Google"
  >}}

>All the posts of this series is available here - [Android without Google!](/tags/android-without-google/)

## Google Plus Alternatives
C'mon who uses Google Plus? Their founders too don't use it! It was _popular_ back then, but no almost no one uses it except me who shares all my posts to _Google+_ which has 1 Follower!

Now, again if you're using _Facebook or Twitter_ then also you're not safe. However, you can turn off Ads Personalisation in Twitter. I don't know how much that works. Still its better to switch!
### Diaspora
Remember something called Disroot, that we discussed in our previous posts? Yes, they also have a Social Network besides Cloud, E-mail and other services.

>__Edit:__ I was mistaken, Diaspora is not owned by Disroot!

Diaspora is based on three key philosophies: _Decentralization, freedom and privacy_. It is intended to address privacy concerns related to centralized social networks by allowing users set up their own server (or "pod") to host content; pods can then interact to share status updates, photographs, and other social data.

That's all I found that can replace Google+ for you! If you have some more, then share in the comments below!

>__Edit:__ Friendica is another Social Network which you might consider, however it is not as popular as Diaspora!

## Google Keep Alternatives
I really love Google Keep, the design is simply awesome. It is a _full-featured_ note taking application with nothing to cry about. The best alternative you'll find is _Evernote_, but trust me it is also not secure!

There are ton of other application that you can use, I'll name a few which syncs online and others which work offline!
### [NoteBuddy](https://f-droid.org/packages/nl.yoerinijs.notebuddy/)
This is for them who have _Android 6.0 or higher_. It is available only on [_F-Droid store_](https://f-droid.org/packages/nl.yoerinijs.notebuddy/). If you've been following the tutorials then, remember we suggested [Google Play Alternative](/how-to-use-android-without-google-part-1) as __F-Droid__?

All notes are stored and encrypted offline. For security password is also not stored, so if you lose your keys then forget them! This will only work offline, so there is no way to access it on your Desktop or Laptop.

You can sync the database on cloud and access your notes in another Android device which is 6.0 or higher.
### [Standard Notes](https://standardnotes.org/)
This is yet another famous note taking application. It is famous for it stores your notes using __AES-256 Encryption__. No one can read your notes, not even them. It is very _simple note_ taking application and have their presence everywhere, including App Store, Play Store,etc. They also have an Web Application, so your notes go with you!

They also have a bunch of themes and custom editors, but those features are restricted to their _Premium_ users only!
### [NextCloud Notes](https://f-droid.org/packages/it.niedermann.owncloud.notes/)
This is a note taking application for you, if you use _NextCloud_. In our previous posts we talked about [Disroot](/android-without-google-part-2), which provides cloud service, E-mail service and many other services. If you need an ecosystem that Google gives but don't want to sacrifice your privacy then sign up for disroot now!

Else if you've your own NextCloud setup in your own servers then download this application to take down notes which will be stored as plain-text in your cloud!
### [Protected Text](https://www.protectedtext.com/)
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/protected-text-ui-alternative-keep.png|Protected Text - Alternative to Google Keep"
  >}}

This is another open source web application which you can use for note taking, they have recently released their Android application on Play Store! Nothing is sent to their servers, your data is __encrypted__ and decrypted in your browser itself.

> Also consider OMNI Notes FOSS, suggested by reader. (F-Droid)

## Youtube Alternatives
There's nothing over here, Youtube handles really huge data and that is not easy to displace. __Vimeo__ is the only one that I would recommend instead of Youtube. At least it doesn't belong to Google. Otherwise what you can do is install [NewPipe](https://f-droid.org/packages/org.schabi.newpipe/).
### [NewPipe](https://f-droid.org/packages/org.schabi.newpipe/)
NewPipe is an _Youtube frontend_ that doesn't require any of Google's API or simply Google Play Services on your device. It fetches information from Youtube's Website and displays it to you, but in that manner Google can still track you! If you have that issue then you can use a VPN, we'll discuss more about a secure VPN Provider in our next post.

Remember it is currently new and is improving, it also supports Video and Audio downloads and doesn't show any Ads.

>Someone in the comments below pointed out some other applications that you all may be interested in, the best that I found from there was SkyTube. There was something more that I found interesting, HookTube uh.. that's somewhat like NSFW Youtube but it is different from this, so I will mention it in other post.

### [SkyTube](http://skytube-app.com/)
What's different from NewPipe is that _SkyTube provides a better interface_. It's quite a big _improvement_, I tested it now on my device. It has a _F-Droid Application_ and also a SkyTube Extra apk that provides you with some extra features.

It is completely _open-source_, they also have an website where they talk about features of the Application. This also doesn't require Google Play Services to be installed. You can see users comments on the videos but downloading videos ain't a feature now. It is a good alternative, I'd recommend you to try it and let us know more about it in comments below!

> I myself use NewPipe with VPN because SkyTube has many features that I need but it also misses many, NewPipe is good for me. I feel that both these Project should merge to provide perfect Youtube client alternative.

>All the posts of this series is available here - [Android without Google!](/tags/android-without-google/)

That's all for this part, I'll publish the next part soon, Subscribe to my Newsletter to stay updated with my posts.
