---
title: "Android without Google - Part 7 - Custom ROMs and Copperhead OS"
date: 2018-01-11T01:04:45+05:30
description: "Rooting..? Nah! Installing Custom ROM is a better way to secure your Android!"
draft: false
slug : /android-without-google-part-7/
tags : ["Android-without-Google", "Privacy"]
comments: ["You forgot to add Dirty Unicorn - Unknown", "@Unknown Thank you for suggesting this! Added! - HorribleGeek"]
aliases:
       - /2017/12/android-without-google-part-7.html
       - /2017/12/how-to-use-android-without-google-part-7.html
       - /how-to-use-android-without-google-part-7

---
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/android-without-google-part-7.jpg|Android without Google"
  >}}

>All the posts of this series is available here - [Android without Google!](/tags/android-without-google/)

_Custom ROMs_ are simply stock android that has been modified. There are many Custom ROMs in the market but I'll list the best ones. Installing wrong ROM for your device may __brick__ it and it'll be unusable, so I'd recommend you to have some knowledge about this topic before going ahead. Most of the popular smartphones support Custom ROMs, but if your device doesn't support Custom ROM then don't be sad just follow Part 6 of my series and your device will be no less than installing a Custom ROM.

With removing Google Applications they also provide better experience and many other great features!
## [Lineage OS](https://lineageos.org/)
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/LineageOS-Logo.jpg|Lineage OS Logo"
  >}}

3 out of 10 Android devices run __Lineage OS__, according to their latest post which was about completion of a year! This is a continuation of __CyanogenMod Project__, which was stopped in late 2016. It now supports over 200 devices and more counting rapidly because of their huge community. Read more about Lineage OS on their official Site.
## [Paranoid OS](http://get.aospa.co/official)
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/Paranoid-Android-Custom-ROMs.jpg|Paranoid - Custom ROM"
  >}}

This is the one which I love, well there is one feature that I liked the most. It's immersive mode, aka fullscreen mode hiding the status bar. It officially supports many devices like _Samsung, OnePlus and more..._ They bring you great user experience with good usage of resources, if your device is supported then I'd suggest you to go with this.

>Image Source: _Beebom_

## [Resurrection Remix](https://sourceforge.net/projects/resurrectionremix/files/?source=navbar)
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/Resurrection-Remix-Logo-New.jpg|Resurrection Remix Logo - Custom ROM"
  >}}

Probably the most preferred ROM in the market, RR has been from a long time. I'd say in simple words it is Paranoid OS + Lineage OS + Many Great Features! = Resurrection Remix!

This makes it lag sometimes, it has many features but the ROM is also __heavy__ and won't be supported on __low end devices__. RR supports all the major manufacturers like _Samsung, OnePlus, Lenevo, LG, Moto_ and many more.. It is the first to push out updates also, probably the fastest to get updates!

That's all, I am not that great in Custom ROMs topic so if you have something better to share then do comment below.
## [Copperhead OS](https://copperhead.co/android/)
The os which supports all the Android Applications, it is the best that you can get for your device. They are __open source__ and also support other open source alternatives for Google like F-Droid which comes pre-installed.
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/copperhead-os.png|Copperhead OS"
  >}}

Currently this supports only _Nexus 5X and Nexus 6P and Pixel, Pixel XL_ is available as a product. It will push out security updates for your device for at least 1.5 years after the last device is sold. It is also very stable, it has to be because it is distributed as a OS. With a stronger encryption this is the best you can get for yourself!
## [DirtyUnicorn](https://dirtyunicorns.com/)
Someone from the comments pointed out this, I check their project and decided to add it here. Acc. to them they say that this project was made to make to stock android experience better all over the world. It is totally _open-source_ and is pretty popular in Android Market, I don't know how I missed this!
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/Dirty-unicorn.jpeg|Dirty Unicorn"
  >}}

Pretty Neat Huh! Don't forget to check it out.
## [Micro-G](https://microg.org/)
Many suggested me to add this also to my article, if you want to use some applications which uses Google's API then you can get __Micro-G__. From the name you can understand that it is Micro version for Google Play Services also it is open-source.

That's all and this will be the last post of this series (probably this time). If you want me to add something or have some better alternative for applications or anything that I've mentioned in this series then comment below!

>All the posts of this series is available here - [Android without Google!](/tags/android-without-google/)

That's all for this part, I'll publish the next part soon, Subscribe to my Newsletter to stay updated with my posts.
