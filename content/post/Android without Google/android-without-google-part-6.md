---
title: "Android without Google - Part 6 - Completely ripping off Google, Uninstalling System Applications"
date: 2018-01-11T01:04:42+05:30
description: "Going to next level - Uninstalling System Applications - Rooting!"
draft: false
slug : /android-without-google-part-6/
tags : ["Android-without-Google", "Privacy"]
comments: ["Please publish an article on rooting android - Unknown", "@Unknown Done! - HorribleGeek", "I don't think removing Google's apps is enough, there is always some malware left. What's best is to install a custom ROM and not install open Gapps from http://opengapps.org - As you can see, even the 'pico' option installs lots of small apps, dependencies and what not and I don't even know how many of those there are, it's very tedious to uninstall at least 50 of them. The best way is to not install them at all after you flash the custom ROM and only then one can be sure there is no Google in one's Android. Also SuperSU is outdated. Magisk is much more recommended in my book- HeyThat'sPrettyGood", "@HeyThat'sPrettyGood Now this is something informative! I'll checkout Magisk and update this article!- HorribleGeek"]
aliases:
       - /2017/12/android-without-google-part-6.html
       - /2017/12/how-to-use-android-without-google-part-6.html
       - /how-to-use-android-without-google-part-6
---
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/android-without-google-part-6.jpg|Android without Google"
  >}}

> All the posts of this series is available here - [Android without Google!](/tags/android-without-google/)

By now, if you have been following my posts then you have an Android device with all the _Google Apps disabled_. Now they are of no use to us, so why keep them on our device? We can completely remove all the applications, but you'll need to __root__ your device for that.

Rooting your device is simply the procedure of gaining privileged control which we call as root access. If you're doing this for the first time, then do it carefully because if anything goes wrong you may brick your device which will be no longer usable!

I'd recommend to read a few articles and then flash __SuperSu__ using any recovery program. You'll understand these terms when you read more about rooting.

It is highly recommended to root using SuperSu because other rooting methods like _Kingo root or King root_ themselves install _bloatwares_. Also, note down one point that SuperSu is a zip file that you will have to flash not apk!

After rooting your Android, you will have a ocean of applications to discover. Always move with caution, because it is not a piece of cake. Now that you have successfully rooted your device, you will have to install Titanium Backup. Here's the Google Pla... wait! we have disabled Play Store. Here the problem starts, you will not find this in F-droid store instead you'll have to download an .apk file and install it manually.

The most recommended site is [Uptodown](http://uptodown.com/), only for _Titanium Backup_. Get it from there and install it, after installing it will ask for root access, grant! Now you are all set just go on rage mode and uninstall all the Google applications!
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/deleting-system-apps.png|Deleting System Applications"
  >}}
This will be your final move, now you are all set. Relax back, your phone is Google Free completely! I'd recommend you to read Part 7 of this Series once. which is on Custom ROMS and other OS.

>All the posts of this series is available here - [Android without Google!](/tags/android-without-google/)

> **[Guide to Root your Android Device](/guide-rooting-your-android-device/)**

That's all for this part, I'll publish the next part soon, Subscribe to my Newsletter to stay updated with my posts.
