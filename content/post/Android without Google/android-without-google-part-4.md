---
title: "Android without Google - Part 4 - Alternatives to Launcher, Contacts, Maps and Calendar"
date: 2018-01-03T23:57:02+05:30
description: "In this article you'll find alternatives to Launcher, Contacts, Maps and Calendar!"
draft: false
comments: ["You didn't mention alternative for G Maps, Maps.me is great! - Anonymous", "@Anonymous Thanks! for the suggestion, I'll add it to the list. - HorribleGeek"]
slug : /android-without-google-part-4/
tags : ["Android-without-Google", "Privacy"]
aliases:
       - /2017/12/android-without-google-part-4.html
       - /2017/12/how-to-use-android-without-google-part-4.html
       - /how-to-use-android-without-google-part-4
---
{{< fluid_imgs
"pure-u-1-1|/img/Android-without-Google/android-without-google-4-1.jpg|Android without Google"
  >}}

>All the posts of this series is available here - [Android without Google!](/tags/android-without-google/)

## Alternatives for Launcher
It is a high chance that you're using that bloated _Google Launcher_, it provides you quick access to some of the great features but that's not worth your Privacy. If you've been using that Samsung's Launcher TouchWiz then also I'd recommend you to switch.
### Nova Launcher
_Edit:_ I have now switched to [KISS Launcher](https://f-droid.org/en/packages/fr.neamar.kiss/) which is open-source and available on F-Droid and I'm lovin' it!
This is the first post where I am discussing about a __non open-source__ launcher. This is also the launcher that I use. If you still have issues about Privacy then you can disable anonymous data collection or altogether disable it using a Firewall application!

>You can always use a Firewall to restrict its access to the internet!

It is a very cool and customisable launcher, I use it because of its low ram usage. It uses less ram than the stock launcher, so I replaced it.

I tried to find some good launchers in F-Droid store and found some pretty good launchers but none of them was able to compete with Nova.
### [Emerald Launcher](https://f-droid.org/packages/ru.henridellal.emerald/)
You might consider this as an Nova alternative, it is a pretty good launcher. It is open-source, supports icon packs and has bunch of other settings. The mind blowing thing is that it is just around __100 KB__. That screenshot is of course not of my device!

## Alternative for Maps
A reader suggested me this, I don't rely on _Map Services_ but it seems that many do. So I am adding this to this post, the best Open-Source alternative to _Google Maps_ is __Maps.me__!
## Alternative for Contacts
The best one that I could find for this is [Davdroid](https://f-droid.org/packages/at.bitfire.davdroid/)! It simply works with NextCloud, so if you can then I'd recommend you to set up NextCloud otherwise you can also use [Disroot](/android-without-google-part-2/#disroot-https-disroot-org) Services which are powered by NextCloud.
## Alternative for Calendar
Well, guess what? You already know this! Yes again its __Davdroid__, it also syncs your _Calendar_ with _NextCloud Server_. So now you've got a bunch of reasons to use _NextCloud_, just start away! It provides you with Notes, Cloud, E-Mail, Tasks and many other services. If you can't just set it up on your own then use Disroot! See my previous articles on this topics!

Still if you don't want to use the stock Calendar application, then go with this one - [Simple Calendar](https://f-droid.org/packages/com.simplemobiletools.calendar/). As the name suggests, it is very simple and also has a dark theme! I really love dark.. web, book etc,.

>All the posts of this series is available here - [Android without Google!](/tags/android-without-google/)

That's all with this part and this is the last part in which we're discussing about replacing applications. In our next post we will discuss about other ways to further harden Android's security.
