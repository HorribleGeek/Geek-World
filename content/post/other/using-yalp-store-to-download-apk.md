---
title: "Downloading Apps from Play Store without actually using it - Yalp Store"
date: 2018-01-12T23:53:06+05:30
description: "Hate Google but Love Play Store..? Then this is for you!"
comments: ["I love Delta Updates from Yalp! - Unknown"]
draft: false
slug : /using-yalp-store-to-download-apk-from-play-store/
tags : ["App-Store", "Android"]
aliases:
       - /2017/12/downloading-apps-from-play-store.html
---
{{< fluid_imgs
  "pure-u-1-1|/img/app-store-imac.jpeg|Yalp Store"
>}}

If you have been following my Guide to use [Android without Google](/how-to-use-android-without-google-part-1/), then I'm sure you've been missing Play Store since long. Don't be sad you can download Applications from _Play Store_ without sending much of data to Google.

I came to know about this from my friend Rad from Reddit. We were having a conversation, where we discussed about [Yalp Store](https://f-droid.org/en/packages/com.github.yeriomin.yalpstore/). The best thing is that this is available in the [F-Droid](https://f-droid.org/en/packages/com.github.yeriomin.yalpstore/) repo and is completely _open source_.

Yalp Store downloads _.apk_ file from Google and lets you install it. __Yalp Store__ uses its own _login to download Apps_, however you can use custom login option which is not advisable. The only thing Google gets to know is basic information about your device and other details which you can minimize by routing it through _Tor_.

Yalp Store also lets you know when the Application is dependent on Google Play Services, also whether it is Ad-Free or not and there are many other basic features. You can even update your applications but please check them while updating, sometimes you may downgrade too! This happened in my case when I was looking to update an Application.
{{< fluid_imgs
  "pure-u-1-1|/img/Yalp-errors-1.png|Yalp Store Bug"
>}}

It was updating this application from 3.3.2 to 3.0.0, there are some error but overall this is something good that you can use without missing those Applications and is actually safe than using a 3rd Party Sites to download Apk and run.

Be careful before downloading or updating your Applications, always check your version and the update version and also the Description! Also another great feature is __Delta Update__ which means that only the changes will be downloaded when updating applications, something that Play Store does.

That's all, Yalp is indeed a great alternative but if you know something better then do share it with us! Also, if you missed my series of posts on Using [Android without Google](/how-to-use-android-without-google-part-1/) then I'd recommend you to check it out once!
