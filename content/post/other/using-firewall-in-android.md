---
title: "Securing your Android - Using Firewall to restrict internet access"
date: 2018-01-13T10:36:37+05:30
description: "Firewall to restrict internet access!"
draft: false
slug : /securing-your-android-using-firewall/
tags : ["Android", "Security"]
aliases:
       - /2018/01/securing-your-android-using-firewall.html
---
{{< fluid_imgs
  "pure-u-1-1|/img/Firewall-AFWall.png|AF Wall"
>}}

I was pretty much shocked to see very less articles on this topic, though there are many but all of them recommend No-Root Applications which simply create a VPN and restrict access.

The Application that we will be talking about will require root access as it alters with your iptables, if you don't have root access then go with this [application](https://play.google.com/store/apps/details?id=app.greyshirts.firewall&hl=en) which is the best I could find for non-root users.

Make sure to check out my most popular series of articles on Using [Android completely without Google](/how-to-use-android-without-google-part-1/)!

I personally use this application, called __[AFWall+](https://f-droid.org/en/packages/dev.ukanth.ufirewall/)__. The best is that they are _open-source_ and available on _F-Droid Store_. I have a low end device, so I prefer using lightweight applications. AFWall+ is extremely _lightweight_ and doesn't eat up your resources as other applications do.
![AFWall +](/img/AFWallPLus.png)
This is the most I have seen it use, P.S - I waited for 10 minutes to capture this! Usually the usage is ~2MB or even less than that. It has pretty good user interface, everything is clear and even a newbie will be able to understand the settings. It also has profiles, you can enable different apps for different profiles!
{{< fluid_imgs
  "pure-u-1-1|/img/AFWall Interface.png|AFWall Interface"
>}}

[AFWall+](https://f-droid.org/en/packages/dev.ukanth.ufirewall/) is an improved version of DroidWall which was dropped by the developer. You can give permissions for VPS, LAN, Broadband, WiFi, etc., You can also protect the application by using their inbuilt App Lock feature for AFWall+, they have also included Uninstall Prevention. It currently supports Android 4.0+ to 8.0 and is in active development.

You can use this to restrict applications from using Internet without your permission, no longer eating up of data by Google Applications. Wait... you are here so in first place you must not be using any [Google Applications](/how-to-use-android-without-google-part-1/).

It also has support for _Tasker and Xposed_ to prevent applications from sending data packets via _Native Download Manager_! This was the best and it seems only one in the market, if you have some other application which outperforms this then do comment below!
