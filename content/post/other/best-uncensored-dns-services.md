---
title: "Best Uncensored DNS Services - Google DNS is a big NO!"
date: 2018-01-13T13:20:22+05:30
description: "Changing my DNS Servers..."
draft: false
slug : /best-uncensored-dns-services/
tags : ["DNS", "Privacy"]
aliases:
       - /2018/01/best-uncensored-dns-services.html
---
{{< fluid_imgs
  "pure-u-1-1|/img/Domain-Name-System.jpeg|Domain Name System -DNS"
>}}

Congrats! if you are using Google's _DNS Servers_ and reading my articles! Well, they may provide you with better service but don't forget its Google, everything is __logged__.

DNS Services like _Google DNS and Quad9_ also have fancy ip address but that is of no matter if you are looking for your __privacy__.

Choosing a good _DNS Service_ is must for everything because going for wrong ones may also lock you out, if your ISP provided DNS blocks any websites you can check for it by visiting the IP Address of website.

Over here in India there is nothing called as privacy every other day I used to see a popup from my ISP which was an Government Body. It was really shocking for me, after looking out for a better DNS Service I shortened this list to 2 Services which are best in terms of your _Privacy_.

> Edit: Added 2 more services to this list, recommended by a reader.

If your ISP also _logs data or blocks websites_ or even _modify your requests_ then you should pick an alternative here.
## [OpenNIC Project](https://www.opennic.org/)
All of the servers are volunteer provided and it completely runs on _donations_. You also get access to new Top Level Domains which can only be accessed using their _Service_ and they are also available for _free registration_. You can look at the list of servers [here](https://servers.opennic.org/) or else go to their homepage to find server closest to you which is sorted based on uptime. Their general uptime is about 95% which is good, you must not experience any sort of delay or slow connection.
{{< fluid_imgs
  "pure-u-1-1|/img/Open-NIC-TLD.png|OpenNic"
>}}

Your data is not _logged nor modified_, however some servers logs __anonymous data__ for some time to improve their service. You can find all those information in their servers page. If you experience downtime then consider changing your servers and choose the best one.
## [DNS Watch](https://dns.watch/index)
This is a __free and uncensored service__, that's what their website says but they don't have a dedicated Privacy Policy page. It is only their Homepage that you can look for because they actually don't have anything in their site. It is all about trust, I'd say this will be better to use instead of going for other fancy names like Google or Quad9.

~~I personally use this just because OpenNIC Server's didn't provide quality service at my place.~~ DNS Watch provides a better and faster service for me. So far I've not noticed any sort of lag or slow connection. They have __DNSSEC__ enabled which means that it will ensure that there is no DNS Spoofing.
## [Uncensored DNS](https://blog.uncensoreddns.org/)
The name says it all, I don't know how I missed this because this is the top result for 'Uncensored DNS'! This service is run by a _private individual_, everything is done by him and it runs on donations. You'll find 2 DNS Servers in their website, I didn't find a _Privacy Policy_ page here also! but you can trust them at least for leaving Google.

The links also in their blog seems to be outdated, I don't know if it is maintained actively or not.

>Edit: I just did a background check of the man behind this project and it seems that this is legit. You can yourself do the research and go for the best.

## DNSCrypt

> Please refrain from using DNSCrypt until further notice (DNSCrypt v2 in development, no official website yet).

This is another popular service, also mentioned in PrivacyTools.io page. This runs on a protocol that validates the authenticity of the page you're visiting using signatures to verify that their is no tampering with data. In simple words, no DNS Spoofing! There is a list of [open resolvers](https://github.com/jedisct1/dnscrypt-proxy/blob/master/dnscrypt-resolvers.csv) that anyone can use, you'll find a list of those. You can go with the nearest one or as you wish.

That's all but relying on these services also won't protect you from your ISP. It's better to choose a better ISP, however changing your DNS is a good move. Still all of your requests are been answered by your ISP and they get the lists of site you visit, to enhance your security further go for a [VPN Service](/android-without-google-part-5/) instead.

If you have a better alternative then do share below and wish you a _Happy New Year_!
