---
title: "[Guide] Flashing a Custom ROM on your Android using TWRP"
date: 2018-01-12T22:12:53+05:30
description: "Flashing Custom ROM :/"
draft: false
slug : /guide-flashing-custom-rom-using-twrp/
tags : ["Flashing", "Android"]
comments: ["If your device supports it, use Magisk Manager instead of SuperSU, it's much better in every term - HeyThat'sPrettyGood"]
aliases:
       - /2018/01/guide-flashing-custom-rom.html
---
{{< fluid_imgs
  "pure-u-1-1|/img/Flashing-android/Custom-ROM.jpeg|Android Rocks"
>}}

Flashing Custom ROMs and Rooting using Recovery are one and the same thing, if you have read my article on [Rooting your Android](/guide-rooting-your-android-device/) then go ahead and follow the same steps!

> Note: Flashing _Custom ROMs_ and _SuperSU_ using _TWRP_ is one and the same. Also, flashing _Custom ROMs_ and _Recovery_ using _ODIN_ are one and the same. You can go with any method, if you are a newbie or confused then continue reading...

First of all you need to find a good _Custom ROM_ for your Device. If you have a modern high end device then there's already many ROMs out there. Select the one you want to flash and proceed to download it. Caution over here, because flashing wrong ROM can brick your device and you can lose it forever.

If you don't have an SD Card or still wondering how to flash a [custom recovery]([Flashing Custom Recovery](/guide-flashing-custom-rom-and-custom-recovery/)) then check my guide on [flashing Custom Recovery](/guide-flashing-custom-rom-and-custom-recovery/). You can flash Custom ROMs the same way as we do for Custom Recovery!

Is that in your SD Card?.. If yes then proceed to reboot into your Recovery. Just Power off then Press and hold _Home Button + Power Button + Volume Up Button_. If you're rebooting into recovery for the first time then there will be a warning sort of.. just accept it and move. Read it and then accept.

> You'll need Custom Recovery like TWRP or CMW for flashing SuperSU zip, so first flash Custom Recovery!

{{< fluid_imgs
  "pure-u-1-1|/img/Flashing-android/TWRP-Main.jpeg|TWRP"
>}}

This is my interface of _TWRP Recovery 3.0.0.0._ Click on _Install_ and then you'll see a list of files and folders that is your internal storage. Now below you'll see Select Storage option, just choose __SD Card__ from there and proceed to navigate to your zip file where you've kept your ROM.

After clicking on the file you will see a new window where it will say to slide a bar below, just read everything over there and proceed. Warning: This is going to format your Internal Storage!
{{< fluid_imgs
  "pure-u-1-1|/img/Flashing-android/Flashing-zip.jpeg|Flashing zip in TWRP"
>}}

In above image I'm flashing SuperSU, it is all the same. The Process will take a little longer than usual so have some patience also disturbing it will cause problems further. If you have an old or cheap SD Card then this might take even more time depending on the size of zip you're flashing.

> [Flashing a Custom ROM and Custom Recovery on Android using Odin/J-Odin](/guide-flashing-custom-rom-and-custom-recovery/)

That's all, this was the best and the easiest method that I've ever tried. If you have something better then do share in comments below. I will ensure that this article gets updated overtime.
