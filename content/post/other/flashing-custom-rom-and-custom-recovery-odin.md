---
title: "[Guide] Flashing a Custom ROM and Custom Recovery on Android using Odin/J-Odin"
date: 2018-01-12T20:14:35+05:30
description: "Flashing TWRP on my Android using ODIN"
draft: false
slug : /guide-flashing-custom-rom-and-custom-recovery/
tags : ["Flashing", "Android"]
aliases:
       - /2018/01/guide-flashing-custom-rom-and-custom-recovery.html
---
{{< fluid_imgs
  "pure-u-1-1|/img/Flashing-android/Flashing-android.jpeg|Flashing Custom ROM and Custom Recovery"
>}}


> The one in the image above is Android's Native Recovery!

I have a low end Samsung device (SM-J110H), it was very hard to find a _Custom ROM_ for this device. Infact it was impossible because I didn't find any that was because there was not any available!

I ended up flashing a __Pre-Rooted ROM__ for my Stock ROM. After doing all that I again followed the process and removed all the Google Applications.. and other stuffs to make it safer.

__Also Read:__ [How to use Android without Google? Ditch Google!](/how-to-use-android-without-google-part-1/)

In this guide I will be using __ODIN__ for flashing Custom ROM and Recovery. ODIN was initially made for Samsung devices only. I was on a mac, so I used another software called as J-ODIN which is simply ODIN for Mac. If you are on Nexus then you have _Nexus Root Toolkit_, for Xperia you have _Flashtool_ and so on..

> The steps for Flashing Custom ROM/Recovery is all same so don't get confused. Just follow this Guide!

> Note: Don't forget to download Samsung USB Drivers also don't forget to restart your System after installing ODIN and other required drivers.

You can also flash Custom ROMs using _Recovery_, I will post that in another article. Once you flash a Custom Recovery which is TWRP in my case you can pretty much do anything from rooting to installing Custom ROMs to updating your Stock ROM.
## Flashing Recovery/ROM via J-ODIN / ODIN
After you finish downloading __ODIN / J-ODIN__, installing is the same as for other applications. In mac you may get something like, the resource is blocked.. in that case settings > Apps and click on open anyway. After all that you will get a User Interface for ODIN.

> Note: Though I'm using Mac, this will work also on Windows and is all same. I have added a note if you need to consider something!

Now we need to go to __download mode__ in your device, just hold the _Power Button + Home Button + Volume Down Button_ all together. You will see a warning, press your _Volume Down_ button once to start download mode which is also known as __ODIN Mode__.
{{< fluid_imgs
  "pure-u-1-1|/img/Flashing-android/ODIN-Mode.jpeg|ODIN Mode in Android"
>}}

After getting into Odin mode simply plug your device in to your system and run Odin. There you will see a _green bar labelled Connected_.

{{< fluid_imgs
  "pure-u-1-1|/img/Flashing-android/Odin-UI.png|ODIN UI"
>}}

After connecting your device click on __PDA or AP__ either one, both are the same and select your Custom ROM/ Recovery file which must be a __.tar__ or __.tar.md5__ file and click on start. In some cases you might get an error which says that you must submit a pit file. If the program prompts to find out _pit_ file then click on OK or else ignore.

>Your Interface may be different but the working is all same.

After starting the operation you will see a blue bar getting filled at the bottom of your Android Logo in Odin mode.
{{< fluid_imgs
  "pure-u-1-1|/img/Flashing-android/Flashing-ODIN-Mode.jpeg|Flashing in ODIN Mode"
>}}

Something like this, the process should usually take around a minute or two to finish. If you are using a low quality connector then this may be slower than usual. After the Process is over you will see success message with a Green bar labelled PASS at the Top.
{{< fluid_imgs
  "pure-u-1-1|/img/Flashing-android/Flashing Completed.png|Flashing Successful"
>}}
Flashing Custom ROMs or Recovery has the same process but once you flash recovery you will not need your Desktop anymore. A Custom Recovery like TWRP can do anything, even install Custom ROM.

> [Guide Rooting your Android Device - Everything you need to know!](/guide-rooting-your-android-device/)

That's all, wasn't that easy. It is not that complicated as we all think but be aware flashing wrong ROMs can brick your device. In most cases it is soft brick which can be recovered, do some research on the ROM that you are going to flash.
