---
title : "How to make WordPress Site completely Static!"
date : "2018-01-28T23:50:06+05:30"
draft : false
tags : ["WordPress", "Static Sites"]
description : "WordPress and Static..? Yes! It is Possible."
slug: /how-to-make-wordpress-completely-static/
comments: ["What do you think about HardyPress..? - HorribleGeek"]
---
{{< fluid_imgs
  "pure-u-1-1|/img/imac-with-website.jpg|Making WordPress completely Static"
>}}

Managing WordPress sites means a lot of work. Almost 27% of the web runs WordPress but that doesn't mean that it is the best. __Static Sites__ are making their way to the top. Many Static Site Generators like _Hugo and Jekyll_ have made Blogging easier and Fun!

Most of the WordPress users complain about slow load times and are always looking for ways to improve their site's load time. I was also one of them, until I switched to a Static Site.

## Static Site and Dynamic Site

Static Site means that the site will be same for everyone, it doesn't matter from which country you visit.. not only country you can take any case.. the content will be all same! If it is changed, then it will be changed for everyone!

Dynamic Sites are those which change their content Dynamically. For Example, Your Facebook Feed. You refresh the page, Facebook servers generate a whole new site for you at your request and sends it to your browser. Every time a site is generated for you!

WordPress does the same, whenever someone sends request to your servers, WordPress generates a site for them.. and this happens each and every time! That means a lot of work for your Servers!

## Introducing HardyPress

> This is not a sponsored post, I came to know about this Service by researching myself!

Your Site's load time will improve to a whole new level, [HardyPress](https://www.hardypress.com) crawls your site and copies all the files in their servers. Now whenever someone visits your site, instead of WordPress generating a new site the content will the showed by HardyPress.

This means that the request will never reach your servers! Only HardyPress will come to re-crawl your site. What visitors see is Pure HTML, nowhere PHP comes.

Yes! your WordPress installation will just act like a Static Site Generator. No Hacking, No Malware, No Slow Load Times!

{{< fluid_imgs
  "pure-u-1-1|/img/HardyPress-Explained.png|HardyPress Explained"
>}}

Here's a quick explanation from them!

The only issue is that _WordPress Native Comments_ will not work, because it requires a database! Instead you will have to  go for 3rd Party Comments System like Disqus, etc,.

They also backup your data regularly, provide free SSL using _Let's Encrypt certificate_. The Support is also very good, I had a small talk with one of their Member and the response was very good.

The Prices are also reasonable, you can even get a _shared hosting_ for site with more than 10k views/Month. It won't matter because WordPress will run only when you log in to your Dashboard! Rest of the time HardyPress responds to all the requests.

If you want your site to load faster but don't have enough funds to get a better hosting then instead downgrade your current plan and go for HardyPress. The expense will be balanced and you will get a faster site!

> I could find only this Company providing this Service at minimal costs, if you know such great Startups then do share!

While this is a great idea but switching to a Static Site completely is super cool!
