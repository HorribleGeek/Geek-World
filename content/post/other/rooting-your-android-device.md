---
title: "[Guide] Rooting your Android Device - Everything you need to know!"
date: 2018-01-12T22:12:52+05:30
description: "Rooting my Android Device - Believe me it's fun!"
draft: false
slug : /guide-rooting-your-android-device/
tags : ["Flashing", "Android"]
aliases:
       - /2018/01/guide-rooting-your-android-device.html
---
{{< fluid_imgs
  "pure-u-1-1|/img/Flashing-android/Rooting-recovery-supersu.jpeg|Rooting Android"
>}}

Nowadays there is usually no need to root your _Android_ if you already have a high end device but if your favourite applications require root access or you want to tweak your device to a whole new level you might want to root your Android.

In this article we will be looking at the _simplest method_ to root your device, which will be using a Custom Recovery - TWRP. I have already published an article on [Flashing Custom Recovery](/guide-flashing-custom-rom-and-custom-recovery/) on your device, so this will be a continuation of that.

> You'll need Custom Recovery like TWRP or CMW for flashing SuperSU zip, so first flash Custom Recovery!

There are many applications out there which provide one-click root but they are not as reliable because they themselves install _bloatwares_ in your devices. I would not recommend you to use those like _Kingroot, Kingoroot,_ etc,. Instead flashing [SuperSU](https://download.chainfire.eu/696/supersu/) is better.

You can get the zip file [here](https://download.chainfire.eu/696/supersu/), after downloading it move it to your _internal storage_ (if not already). I have assumed that you already have some knowledge about flashing files, I don't take any responsibility if you brick your device or anything of that sort.. However if you follow this guide, it is very unlikely to happen.

You will see __SuperSU__ in Play Store but downloading that won't do anything if you haven't __rooted__ in first place also don't be smart and root with other application and later install _SuperSU_. That won't work, download the _zip_ file and get ready to flash it using your _Custom Recovery_ which is TWRP in my case.
## Flashing SuperSU using Custom Recovery - Rooting
After downloading the file, shut down your device. Then go to recovery mode by pressing and holding _Home Button + Power Button + Volume Up Button_. If you are doing this for the first time then your recovery might show some sort of warning, read it carefully and proceed. I'm on TWRP so the instructions will be for that however it will be similar to any other Recovery that you are in.
{{< fluid_imgs
  "pure-u-1-1|/img/Flashing-android/TWRP-Main.jpeg|TWRP UI"
>}}

Click on Install, then navigate to your SuperSU zip location. If your file is in SD Card then click on select storage to change it and then navigate to SuperSU zip file. Then click on it, your recovery will ask you to confirm. Click or slide to confirm, then sit back and pray that all goes well. This will usually take less than a minute unless you're on some old SD Card.
{{< fluid_imgs
  "pure-u-1-1|/img/Flashing-android/Flashing-zip.jpeg|Flashing zip in TWRP"
>}}

After SuperSu is flashed, reboot to your system and check the installation using some root checker application.  If everything is done properly then Root Checker will show positive results or else you can always try again. You can also get their Pro Version if you want to support developers work.

> [Flashing a Custom ROM and Custom Recovery on Android using Odin/J-Odin](/guide-flashing-custom-rom-and-custom-recovery/)

That's all, this was the best and the easiest method that I've ever tried. If you have something better then do share in comments below. I will ensure that this article gets updated overtime.
