---
title: "Modern Material Design Blogger Template - Material"
date: 2017-12-17T14:56:56+05:30
description: "Switching to Material Design Theme for Blogger!"
draft: false
slug : /modern-material-design-blogger-template/
tags : ["Blogger-Templates", "Material-Design"]
aliases:
       - /2017/12/modern-material-design-blogger-template.html
---
Acc. to WikiPedia -

__Material Design__ (codenamed Quantum Paper) is a design language developed in 2014 by Google. Expanding upon the "card" motifs that debuted in Google Now, Material Design makes more liberal use of grid-based layouts, responsive animations and transitions, padding, and depth effects such as lighting and shadows.

{{< fluid_imgs
  "pure-u-1-1|/img/Material-Design-1.png|Material Design"
>}}

Google announced Material Design on June 25, 2014, at the 2014 Google I/O conference.

Nowadays everything is switching over to _Material Design_. We have many WordPress themes made on Google's own Material design policies. Many websites have switched over to _Material Design_, so I also made the change. Yes, the current theme on Geek's World!

>This Post was written when this blog was on _Blogger_, I have switched right now to static theme. You can see the __screenshot__ below!

As of 2015 most of Google's mobile applications for _Android_ had applied the new design language, including _Gmail, YouTube, Google Drive, Google Docs, Sheets and Slides, Google Maps, Inbox, Google+, all of the Google Play-branded applications, and to a smaller extent the Chrome browser and Google Keep_.

The desktop web-interfaces of Google Drive, Docs, Sheets, Slides and Inbox have incorporated it as well. More recently, it has started to appear in Chrome OS, such as in the system settings, file manager, and calculator apps.

Google has switched all over to __Material design__. From Youtube to Adsense Dashboard, everything is redesigned. Stay Updated with Google Design - [Click Here](https://design.google).

This theme was made by _zkreations_ - You can find the link below, they are developers from spain it seems, because the whole theme was coded in __Spanish__.
!{{< fluid_imgs
  "pure-u-1-1|/img/Material-Design-Theme-EG.jpeg|Material Design Theme - Material - Blogger"
>}}

<center><button class="pure-button">[Official Link](https://www.zkreations.com/2016/09/material-plantilla-personal-responsive.html)</button></center>

The above theme was an example, this was the theme I used when Geek's World was on Blogger! The theme link is in Spanish, also they have released version 2.0 which is completely different from version 1.0(ref. Screenshot). If you want to get that which is translated in English by me then ping me or comment below.
