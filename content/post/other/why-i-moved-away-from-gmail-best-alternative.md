---
title: "Why I moved away from Gmail - Best Alternatives"
date: 2018-01-13T23:59:52+05:30
description: "G Mail is good but not worth your Privacy!"
comments: ["While some of these services like Boum are US Based but that doesn't matter if you don't store anything! - HorribleGeek", "I use A/I, their service is great. - Unknown"]
draft: false
slug : /why-i-moved-away-from-gmail-ditch-google/
tags : ["Google", "Privacy"]
---
{{< fluid_imgs
  "pure-u-1-1|/img/Gmail-full-ditch-google.jpeg|Ditch Google - GMail Interface"
>}}

> Did you know that Google _scans all your mails_ for Spam? Well, that is okay but it goes further more. It scans your mails to build your __advertising profile__. Yes, all your mails is being scanned by bots and they build your advertising profile out of that. Now, this is a serious issue. Why will we want someone to read our mails? Whether it is a bot or a human.


>One more thing that I will like to clear out is, here are not specifically talking about privacy and anonymity but something beyond that!

Using Gmail is the worst thing you can do to save your _Privacy_. I have already discussed on this topic in one of my posts on [Android without Google](https://geekworld.coregossip.com/android-without-google-part-2/). There we have discussed about alternatives like <a href="https://geekworld.coregossip.com/android-without-google-part-2/#disroot-https-disroot-org">_Disroot_</a> , <a href="https://geekworld.coregossip.com/android-without-google-part-2/#tutanota-https-www-tutanota-com">_Tutanota_</a>, <a href="https://geekworld.coregossip.com/android-without-google-part-2/#proton-mail-https-www-protonmail-ch">_Proton Mail_</a> and <a href="https://geekworld.coregossip.com/android-without-google-part-2/#startmail-https-www-startmail-com-en">_Start Mail_</a>. All of them were Private mailboxes, in this post we will discuss about __Privacy Focused Mailboxes__.

There is a difference in both of them, the ones we mentioned earlier provided you with service but they asked for some of your Identifiable Information. Here we will discuss about Mailboxes which are completely _Private and Privacy Focused_, they just create an _E-Mail account_ for you and nothing else! No backup account, Nothing!

> These can be called as _Anonymous_ Mail Services.

Want to delete your Google Account..? Check this [guide](https://support.google.com/accounts/answer/61177?hl=en) from google.
## [Riseup.net](https://riseup.net)
They provide you with many services like Email, Chat, VPN, Newsletter Service... It is not easy to get an account over here because only _existing users can invite you_. Once you get an account, the invite code and all the other details are removed. Your account can also be deleted if you send bulk mails or spam.
## [Autistici/Inventati](http://autistici.org)
autistici.org and inventati.org offers services to no commercial, no racist, no nazi, no fascist, no party, no organization with his own structure (who have much money..), no sessis.

They provide E-Mail account, web hosting, Newsletter service... It is not easy to get an account over here also, you are introduced with a form from where you will get a link and your request will be processed over there. It will be checked manually by a human. I personally use their Mail service, it is powered by _roundcube_ and once you get an account you can make upto 5 aliases.

They also have many domains setup for their email. Few of them are _cryptolab.net_, _autistici.org_, _privacyrequired.com_...

> Their servers were once compromised in 2004 and they realised this a year later on 21st June 2005. Since then they have improved a lot and still encourage new requests for account.

## [Boum](https://boum.org)
This service provides you with Mailbox, the website is in french and new registrations are closed for a moment. Here's what they say about themselves.... (translated)
{{< fluid_imgs
  "pure-u-1-1|/img/boumdotorg.png|Boum"
>}}

## [AnonyMail](http://syst3k.com/AnonyMail/landing.html)
AnonyMail is an __anonymous mail__ system to protect your privacy and your identity. It allows anyone to send and receive mails from anyone,  via webmail (you can login from normal web site or by __tor__ hidden service)  or with an email client directly from your PC, using also the Tor hidden service with a client correctly configured to work with tor.

_AnonyMail_ cost is very very low, just $4,50 per month, and you can buy it directly from this store.
## [Aktivix](https://aktivix.org)
_Aktivix_ offers Email, Mailing lists and VPN. For requesting a service you need to give proposer email because only existing users can invite new users to use their service.

They have Privacy Policy and ideas similar to _Autistici/Inventati_. Unlike A/I it is powered by _Horde_ which has better User Interface.
## [Resist!ca](https://new.resist.ca)
_Resist.ca_ is a project of a small group of Vancouver-area _activists_ known as the Resist Collective. They also have ideas similar to A/I and offer Email, Web Hosting and Newsletter Service.

>Please note that Resist.ca is not an internet service provider per se, but rather a political project whereby we create and support infrastructure for activists around the world. We have limited resources. Unfortunately, that means we don't have the ability to provide services to everyone that would like them. Keep this in mind as you fill out the application form and remember that we will use the information you provide to gauge whether or not we want to support the work that you are doing with our limited labour.

It is powered by _RoundCube and Squirrel Mail_. They run on donations and encourage donations at least once a month.

That's all, if you want to read about more such services then head over to A/I page. [Link](https://www.autistici.org/links)

> Remember: These services don't ensure full security, they themselves say that your E-Mails are insecure. It is always better to encrypt your data before sending to servers.

> Oh! I forgot to add this:

{{< fluid_imgs
  "pure-u-1-1|/img/lol-google-tracks-you.png|Google Tracks You!"
>}}

He has a point though...

> These services go beyond Privacy and anonymity, if you are looking for a full fledged alternative then go for either of these-

- Disroot
- StartMail
- Posteo.de
- Tutanota
- ProtonMail
- MailFence
