---
date: 2018-01-31 00:05:43 +0530
slug: /how-i-added-share-icons-to-my-hugo-site/
description: "Adding Share Icons to my Hugo Site!"
tags:
- Hugo
title: "How I added Share Icons to my Hugo Site - After every Article"
comments : ["Isn't that cool..? - HorribleGeek"]
---
{{< fluid_imgs
  "pure-u-1-1|/img/social-network.jpg|Share Icons!"
  "pure-u-1-1|/img/Share-Icons/Share-Icons.png|Share Icons!"
>}}

That looks good right..? Yes it is _Plain_ and __Simple__ but adding those Huge Share Buttons on the sidebar with bulky codes won't help either. If someone wants to share it then it won't matter if it is Plain icons or Huge buttons!

As anyone would do, I also went will other options like _AddThis and ShareThis_! Those were just loading that extra 100Kb of Script for actually no reason. So I decided to make my own and place them at the end of each Article!

I Implemented these on my Hugo Site but this will work on any plain HTML content also! First I started with creating a `share.html` file in my `partials` folder. That file had everything, CSS was just a few lines and rest was HTML. I inlined CSS using `style` tag.

{{<highlight html>}}
<h4><i class="icon-share-alt"></i>&nbsp;Share!</h4>
<ul class="share-buttons">
	<li><a href="https://www.facebook.com/sharer/sharer.php?u={{ .Permalink }}" target="_blank" title="Share on Facebook"><i class="icon-facebook" aria-hidden="true"></i><span class="sr-only">Share on Facebook</span></a>
	</li>&nbsp;&nbsp;&nbsp;
	<li><a href="https://twitter.com/intent/tweet?source={{ .Permalink }}&via=HorribleGeek" target="_blank" title="Tweet"><i class="icon-twitter" aria-hidden="true"></i><span class="sr-only">Tweet</span></a>
	</li>&nbsp;&nbsp;&nbsp;
	<li><a href="https://plus.google.com/share?url={{ .Permalink }}" target="_blank" title="Share on Google+"><i class="icon-google-plus" aria-hidden="true"></i><span class="sr-only">Share on Google+</span></a>
	</li>&nbsp;&nbsp;&nbsp;
	<li><a href="http://www.tumblr.com/share?v=3&u={{ .Permalink }}" target="_blank" title="Post to Tumblr"><i class="icon-tumblr" aria-hidden="true"></i><span class="sr-only">Post to Tumblr</span></a>
	</li>&nbsp;&nbsp;&nbsp;
	<li><a href="http://pinterest.com/pin/create/button/?url={{ .Permalink }}" target="_blank" title="Pin it"><i class="icon-pinterest-p" aria-hidden="true"></i><span class="sr-only">Pin it</span></a>
	</li>&nbsp;&nbsp;&nbsp;
	<li><a href="http://www.reddit.com/submit?url={{ .Permalink }}" target="_blank" title="Submit to Reddit"><i class="icon-reddit-alien" aria-hidden="true"></i><span class="sr-only">Submit to Reddit</span></a>
	</li>
</ul>
<!-- Replace {{ .Permalink }} with URL-->
<!-- Style for Icons, Inlined for less requests-->
<style>
	ul.share-buttons{
	  list-style: none;
	  padding: 0;
	}

	ul.share-buttons li{
	  display: inline;
	}

	ul.share-buttons .sr-only{
	  position: absolute;
	  clip: rect(1px 1px 1px 1px);
	  clip: rect(1px, 1px, 1px, 1px);
	  padding: 0;
	  border: 0;
	  height: 1px;
	  width: 1px;
	  overflow: hidden;
	}
</style>

{{</highlight>}}

> Code: [GitLab Snippet - Share.html](https://gitlab.com/snippets/1696265)

This was the code for `share.html` file in `partials` folder. Over there you will see that `{{ .Permalink }}` is called everytime, if you are using this in other site then replace that with your Site URL. If you are using Hugo then it will be replaced by the URL Automatically while generating site.

Also don't forget to replace icons class, I am using Font Awesome for this Project. After this simply use `{{ partial "comments.html" . }}` to call it whenever you want to have those share icons. I added this to my `single.html` file, which was the framework for my articles.

Hugo will replace `{{ partial "comments.html" . }}` with the HTML Code in that file, so that you don't have to type in those codes again and again, also this makes your project look neat.

Using this or any other option takes up very less space and has no effect on Site Load Time!

> Read More:[ How I Implemented Comments on a Static Site - Static Site with Super-Powers](/how-i-added-comments-to-my-static-site/)
