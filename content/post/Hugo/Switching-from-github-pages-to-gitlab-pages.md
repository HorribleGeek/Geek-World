---
date: "2018-01-20T22:04:26+05:30"
title: "Switching from GitHub Pages to GitLab Pages with Hugo"
description: "My Journey from GitHub to GitLab."
draft: false
slug : /switching-to-gitlab-pages-with-hugo/
tags : ["Hugo", "GitLab"]
comments: ["GitLab is Awesome. - HorribleGeek"]
---
{{< fluid_imgs
"pure-u-1-1|/img/GitLab_logo.png|GitLab Logo"
  >}}

GitHub Pages was really cool for my projects. I had a _Student Developer Pack_ so _Private Repositories_ was not an issue but that would have not been for too long, after 5-6 years I had to pay for it. After being with GitHub for a while I discovered GitLab, believe me it is awesome!

First we will discuss about GitLab Pros over GitHub, then Cons over GitHub and then I will share how I am using GitLab Pages and my experience with GitLab Pages.
## Pros
### Shared Runners
The main reason for this switch was the ability to use _Shared Runners_, I could generate my site just by changing the files online. Before I had to make the changes, then run the `hugo` command to generate the site and then push it online.

Now the workflow has become a lot simpler, I just make the changes and push it online. There GitLab comes and generates the site for me and the changes are live within a minute or two.
{{< fluid_imgs
"pure-u-1-1|/img/Hugo/GitLab-Site-Generation.png|GitLab Shared Runners"
  >}}

Most of the time the generation takes around 90 seconds but that is a lot less for other sites that I've seen. Maybe because I use a heavier image, I will work on that later to reduce that.
{{<highlight yml>}}
image: monachus/hugo

variables:
  GIT_SUBMODULE_STRATEGY: recursive

pages:
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
{{</highlight>}}
I am using this file for .gitlab-ci.yml, I copied this code from Hugo's Official Website. Want to help..? Then go to my [GitLab](https://gitlab.com/HorribleGeek/Geek-World/issues) Repo and create a [new issue](https://gitlab.com/HorribleGeek/Geek-World/issues), I'll merge your changes!
### Multiple Custom Domain
Custom Domains are shared by both of them but Multiple Custom Domains is only available on GitLab, though it doesn't perform 301 redirects instead your site is live on multiple domains!

> Edit: After doing some research I learnt that you can make multiple domains behave like aliases by pointing the CNAME of one to other. Read the [Official Documentation](https://docs.gitlab.com/ee/user/project/pages/introduction.html#add-a-custom-domain-to-your-pages-website) for better understanding.

### Secured Custom Domain
GitHub provides you with secured domain if you use their subdomain for your site, however if you decide to use Custom Domain then you will have to compromise on __SSL__. Many of the users solve this by using _CloudFlare for SSL_. I also did the same when I was using GitHub Pages.
### Support for Static Site Generators
GitHub is cool if you use Jekyll because it has native support for _Jekyll_ sites but if you want to go with other SSG (Static Site Generators), then you will have to generate it offline and push it to GitHub.

Whereas GitLab supports most of the SSGs natively, so no need for you to generate your site offline.
### Jekyll Plugins
GitHub supports Jekyll but most of the plugins are restricted to use, only selective plugins are available for you to use. GitLab supports all the Jekyll Plugins!

_Note:_ I have not used Jekyll so I don't have much idea on this topic.
### Importing & Exporting Repositories from/to other sources
- GitLab can import projects and issues from more sources (GitHub, BitBucket, Google Code, FogBugz, Gitea and from any git URL) than GitHub or any other VCS.
- GitLab allows you to export your project to other systems.

### Design
GitLab has Responsive design and works like a charm on mobile devices. I have edited some of my posts on my mobile device which an old device from 2015. (Small Screen size)

Editing anything on GitHub on mobile devices..? Just forget it! I cannot even add a new Issue without any issue! GitLab also has an Android Application, though with not many features but it has basic features.
### Unlimited Private Repositories
This is a feature that I will miss after my Student Developer Pack expires. GitLab lets you create _Unlimited Private Repositories_ without any restriction.
### Restrictions
GitLab gives you 10GB of storage per Repository whereas GitHub gives only _1GB_ with a _limit of 100 MB per file_ also with a Bandwidth limit of _100GB per month_.

GitLab's only restriction is 10GB of storage, there is no restriction on Maximum file size or Bandwith!
_Note:_ The restriction is only for Private Repositories and not for Public Repositories.
### Open-Source
GitLab itself is Open-Source, you can download the source and deploy it on your own server with no restriction!
## Cons
### Popularity
GitHub is the most popular git repository with the largest number of users and projects. You are most likely to find a developer over there than on GitLab. This was because GitHub was the first in the market and GitLab entered very late.
### Speed
GitLab is trying to keep up with the pace but they haven't yet defeated GitHub and I don't see that happening in the near future. Push and Pulls are faster on GitHub, also if you are going to use GitLab Pages then get ready to face high server response time. Actually not that high, 99.99% of their requests are responded back under 1 second.

To solve this I had to use CloudFlare. I made page rules for Edge Cache TTL and used Cache Level as everything. Now all the requests are responded by CloudFlare and when I update my content I simply purge the CloudFlare Cache! Simple!
{{< fluid_imgs
"pure-u-1-1|/img/Hugo/CloudFlare-Page-Rules.png|CloudFlare Page Rules"
  >}}

These are my Page Rules for this blog.

> I don't see any other major issue for not using GitLab as an alternative for GitHub! These were the major reasons that I thought about before leaving GitHub.

My setup with GitLab is pretty much simple. I am using CloudFlare as a CDN and nothing else for improving my site. Everything works well, CloudFlare is used just for faster response time and SSL. I would recommend you to do the same if you find speed as an issue.

Soon after switching to GitLab Pages I got adapted to it, the interface is very simple and easy to adapt. If you are thinking about making a switch then do try GitLab once, I'm sure you'll love it!

## Sources
- [GitLab Compared](https://about.gitlab.com/comparison/)
- [GitLab Pages Compared](https://about.gitlab.com/comparison/gitlab-pages-vs-github-pages.html)

> Edit: After writing this article I've made a lot of changes to my blog. I have migrated from CloudFlare as a CDN to Netlify as a CDN, though DNS is still handled by CloudFlare.
