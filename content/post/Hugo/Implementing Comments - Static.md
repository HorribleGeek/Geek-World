---
date: 2018-01-24 16:33:43 +0530
tags: ["Hugo"]
title: "How I Implemented Comments on a Static Site - Static Site with Super-Powers"
comments: ["Go ahead, add your comment! - HorribleGeek", "Lol! I'm going to steal this. - Unknown", "@Unknown Sure - HorribleGeek", "I REALLY want to see how this comments of mine is displayed, to check if I can use this system on my own hugo websites. It doesn't allow threaded comments, right? Thanks for the post. - Marco", "@Marco Yes for now this doesn't allow threaded Comments but you can implement this. I don't have any plans to do that now, you can simply tell your users to add '@xyz' and you will have to manually inform xyz about this. This is a bit of work to do but works fine for my site :/ BTW what is your blog address? - HorribleGeek", "@HorribleGeek Hello, and thanks for asking. My main blog is http://stop.zona-m.net . The 'Other articles' box in the sidebar links to some of my other Hugo-powered websites - Marco", "@Marco I'm sorry for the late reply, I'll check your Blog ;) - HorribleGeek"]
slug: /how-i-added-comments-to-my-static-site/
draft: false
description: "Implementing Static Comments on a Static Site!"
---
{{< fluid_imgs
  "pure-u-1-1|/img/Hugo/Hugo-is-awesome-and-so-am-i.jpg|Hugo is awesome!"
>}}

Hugo was really awesome but I had to look for a Comment System. **Staticman** and **Remarkbox** were great but this blog is hosted on GitLab's Repo, so no Staticman support and Remarkbox was not that great option.

I wanted something like Staticman, where I own the data. Using Disqus or anything else like that was not an option for me because I want to have my data and also I didn't like that bulky js.

While searching for another option I read this post by [Joel Dare](https://www.joeldare.com/blog/post/comments-on-a-static-site/), he talked about how he implemented Comments on his Static Site. He added a link to the bottom of the post which will ask the user to send him a mail. Then he manually adds the comments in the Front Matter of the posts.

    ---
    date: "2018-01-20"
    title: "Comments on a Static Site"
    comments: [
        "This is really awesome."
    ]
    ---

Something like this, after looking at this post I also had a great idea. My site is using Netlify as CDN and I use Netlify Forms on the Contact Page of my site. My idea was to make a form and place it at the bottom, whenever someone fills that forms and clicks submit - I get a mail and then I add the comment in the Front Matter of the post.

This was a great idea, if at all Netlify Forms closes down then I had to change just a line of code and my Comment System will be up and running with Formspree or any other system.

## Implementing the Comment System

> Edit: After writing this I've made a lot of changes for comment css.

{{< fluid_imgs
  "pure-u-1-1|/img/Hugo/My-final-comment-system.png|Implementing the Comment System!"
>}}

This was the final output, after finishing off everything! I am using Hugo as a static site generator but the code and steps will be the same for any, just the path of file will change.

First I made a ````comments.html```` file and placed it in the partials directory. Most of the content for this file was taken from Joel Dare's implementation.
{{<highlight go>}}
    <div>
        <h2 style="margin-top: 50px;">Comments</h2>

        <ul style="margin-top: 20px;">
            {{ if .Params.Comments }}
                {{ range .Params.Comments }}

      <div class="dialogbox">
        <div class="body">
          <span class="tip tip-up"></span>
          <div class="message">
            <span> {{ . }}

            </span>
          </div>
        </div>
              {{ end }}
            {{ else }}
                 <div class="dialogbox">
        <div class="body">
          <span class="tip tip-up"></span>
          <div class="message">
            <span> There are no comments yet.

            </span>
          </div>
        </div>
            {{ end }}
        </ul>
        <h3>Add your comment!</h3>

          <div id="comment_form">
          <!-- I am using Netlify, you will have to change this accordingly -->
       <form name="Comments | {{ .Title }} " action="/thank-you-for-commenting" netlify>
      <p>
        <input type="text" name="name" placeholder="Your Name">
      </p>
      <p>
        <input type="email" name="email" placeholder="Your Email">
      </p>
      <p>
      <h6> This comment may take some time to appear. Markdown Supported!</h6>
        <textarea name="comment" placeholder="Comment" rows="7"></textarea>
           </p>
      <p>
        <button type="submit" class="btn btn-primary">Send</button>
      </p>
    </form></div></div>
{{</highlight>}}
This was the framework of the comment system, next I had to add CSS to make it look like cool!

I took CSS for my Comments from [Talented Unicorn - CodePen](https://codepen.io/talentedunicorn/pen/rEqsf "Talented Unicorn - CodePen").
{{<highlight css>}}
    .tip {
      width: 0px;
      height: 0px;
      position: absolute;
      background: transparent;
      border: 10px solid #ccc;
    }
    .tip-up {
      top: -25px;
      left: 10px;
      border-right-color: transparent;
      border-left-color: transparent;
      border-top-color: transparent;
    }
    .dialogbox .body {
      position: relative;
      max-width: 300px;
      height: auto;
      margin: 20px 10px;
      padding: 5px;
      background-color: #DADADA;
      border-radius: 3px;
      border: 5px solid #ccc;
    }
    .body .message {
      min-height: 30px;
      border-radius: 3px;
      font-family: Arial;
      font-size: 14px;
      line-height: 1.5;
      color: #797979;
    }
{{</highlight>}}
Then CSS for my Comment form was taken from [CSS Deck](http://cssdeck.com/labs/simple-css3-comment-form "CSS Deck").
{{<highlight css>}}
    #comment_form input, #comment_form textarea {
    	border: 4px solid rgba(0,0,0,0.1);
    	padding: 8px 10px;

    	-webkit-border-radius: 5px;
    	-moz-border-radius: 5px;
    	border-radius: 5px;
    	outline: 0;
    }

    #comment_form textarea {
    	width: 350px;
    }

    #comment_form div {
    	margin-bottom: 8px;
    }
{{</highlight>}}

After all these I was ready with the final output. Now whenever someone fills the form and clicks on send button, the request is mailed to me and I add the Comment in the Front Matter of the Post.

> If you are following this article then don't forget to add ````{{ partial "comments.html" . }}```` to single.html

That article was a life saver, otherwise I would have gone for other solution like Disqus. This has a little bit of work to do but it is fine for my blog where the comments are very less.

> Also Read: [How I added Share Icons to my Hugo Site!](/how-i-added-share-icons-to-my-hugo-site/)

> I am now using Yahoo's Pure CSS for my comment form.
