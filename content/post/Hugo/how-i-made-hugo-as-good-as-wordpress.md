---
date: 2018-01-24 16:33:43 +0530
slug: /how-i-made-hugo-as-good-as-wordpress/
description: "Hugo was already awesome but then I went to the extent to customize my workflow and now writing posts in Hugo is as simple as doing it in WordPress."
tags:
- Hugo
title: How I made Hugo as good as WordPress - Hugo is Awesome
comments : ["This is really awesome. - Horrible Geek", "I've also worked with Forestry.io and it is great! - Not Known", "@Not_Known Indeed it is Awesome! - HorribleGeek"]
---
{{< fluid_imgs
  "pure-u-1-1|/img/Hugo/hugo-better-than-wordpress.jpg|Hugo is awesome!"
>}}

It was a pain to write my posts in markdown and then push it. Now I write all my articles in a cool modern text editor and push it to GitLab, there Netlify comes and builds my site and the changes are live within a minute or two.

> Forestry.io also builds your site and publishes it to public folder.

> This support is only available for Hugo and Jekyll.

Using [Forestry.io](/go/forestry.io) makes it really very easy to publish new articles. I have a free account, which is enough for me. The support is awesome!

I bugged them for every issue I had and they responded everytime with the solution.

Overall I had a very good experience with Forestry. I am very happy leaving WordPress and Blogger behind.

### My Workflow

* I write posts on Forestry(Cross-Platform) / Atom(Mac) / iA Writer(Android)
* I push the changes to GitLab
* Netlify detects the changes and rebuilds my site.
* My site is live within 2 minutes.

All I have to do the first step with Forestry.io and rest is automated. Huh... So static sites are not static anymore!

> Static Sites with Super-Powers.

> Read More:[ How I Implemented Comments on a Static Site - Static Site with Super-Powers](/how-i-added-comments-to-my-static-site/)
