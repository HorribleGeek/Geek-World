---
title: "Hugo Is Awesome - GoodBye Blogger"
date: 2018-01-13T15:02:25+05:30
draft: false
slug : /hugo-is-awesome-goodbye-blogger/
description: "From WordPress to Blogger to Hugo, It was Fun!"
tags : ["Hugo"]
comments: ["Where do those performance screenshot come from? Thanks - Marco", "@Marco I use Pingdom and GTMetrix for this! - HorribleGeek"]
---
{{< fluid_imgs
"pure-u-1-1|/img/Hugo/Html-hugo-is-awesome.jpeg|Hugo is Awesome!"
  >}}

Blogger was really cool, you get a nice UI and you start writing. Well, that was cool but what about all those content..? You don't _own_ them and they all _belong to Google_! There comes [__Hugo__](http://gohugo.io/), this blog is setup on __Github Pages__ with Hugo as generator and __Cloudflare__ as CDN.

> As of 19th January 2018, I migrated this blog to [GitLab Pages](/switching-to-gitlab-pages-with-hugo/) and I'm lovin' it.

I just wanted a _faster and better blog_, Hugo is simply _Awesome_! Writing blog posts is not as hard as you might think and also you will learn a lot if you are a newbie. Just look at a average Blogger site-

{{< fluid_imgs
"pure-u-1-1|/img/Hugo/Bad-Site-blogger.jpg|Blogger Site PageSpeed"
  >}}

Of course your site may not be this bad, nor was my blog. Still, it is simply __Bulky__.. I used a _minimal theme_ with no other plugins still the scores were not that good and it used to take 4 seconds to load! What about this site..?
{{< fluid_imgs
"pure-u-1-1|/img/Hugo/Awesome-Site-Hugo-GeeksWorld.png|Hugo Site PageSpeed"
  >}}

That is _freakin' fast.._ only __200ms__ to load and the site is also only __300 kb__! All the requests are from my domain and 95% of the content is _Image_! Woah, that is only _5% of script_. This is what I was looking for. Another reason to stay with Blogger was simplicity but that is not an issue once you get used to writing in Markdown.

Hugo is _properly documented_, you will learn everything just by reading the official documentation and also has a _Huge community_, you write posts in Markdown which is then converted to HTML by Hugo and you get a static site generated every time with just 4 characters "hugo".
{{< fluid_imgs
"pure-u-1-1|/img/Hugo/writing-posts-in-hugo.png|Writing Articles in Hugo"
  >}}

This is how I write my articles, wasn't that cool? Markdown is also very easy to learn and customising Hugo is simply easy. Did you see how much time did the _generation take._.? Only __186ms__, that is less than the time my site takes to load. Hugo is written in _Go_ and supports _multi-threading_. You regenerate your whole site even for a small change. You can also see the changes locally before pushing them online by using "hugo server".

Hugo also has a lot of themes for you, ranging from a Landing Page to a Blog you'll get it all. Changing themes is also a piece of cake, you just have to change the name in config file and you have a whole new theme. No! That doesn't mean that you will change your theme every _10 seconds_, you can do that with Hugo!

> Most importantly-  __You own your Content__!

In this manner you can start your own blog for free. Just get a _GitHub account > download Hugo > generate your site > push it to Github > configure Github Pages_ and you are all set.. your site is live!

If you want a custom domain then you'll have to pay for that, That's all! No more Charges, for better functionality use Cloudflare as a CDN and make it HTTPS! There are many other _Static Site Generators_ like __Jekyll__ but nothing is as fast as Hugo!

That's it! Super fast and easy, no worries! I've used _WordPress (Self-Hosted & .com both) and also Blogger_ but nothing seems as better than [Hugo](http://gohugo.io/). I am going to write an series of articles on this topic, __Hugo is Awesome__!
{{< fluid_imgs
"pure-u-1-1|/img/Hugo/static-websites-rock.png|PageSpeed Static Site"
  >}}

> This was my new page load time after switching to a minimal theme! 95ms!
