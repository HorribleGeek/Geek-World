---
title: "List of Awesome FOSS Applications from F-Droid Store"
date: 2018-02-08T13:20:22+05:30
description: "I love FOSS, here is a list of Awesome FOSS Applications from F-Droid!"
draft: true
slug : /list-of-awesome-foss-applications-from-fdroid/
tags : ["FOSS", "Android"]
---
There are many Awesome Apps on F-Droid that you'll never know until you download them. Currently I am using only FOSS Apps on my Android. No ~~Micro-G~~ also! My experience so far has been good with FOSS Apps that are available on F-Droid. Over here I'll be sharing all the Apps that I use and also some other that are Awesome!

<!-- To be Updated-->
