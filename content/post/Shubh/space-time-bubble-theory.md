---
title: "Space Time Bubble Theory"
date: 2018-01-12
description: "I know a bit about Cosmology!"
draft: false
slug : /space-time-bubble-theory/
tags : ["Space", "Bubble-Theory"]
comments: ["Boys and Girls, all of us are from the same Bubble! - Shubh"]
---
{{< fluid_imgs
"pure-u-1-1|/img/earth-space.jpeg|Space Time Bubble Theory"
  >}}

>Hey everyone, I'm Shubh and this is my first post on Geek's World!

The __Big Bang Theory__ says that all the matter and energy which we see around was concentrated at a _single point_ which due to the _Super Force_ expanded with a speed more than the speed of _light(300000000 m/s)_. This gave rise to millions of stars and galaxies but the _Big Bang Theory_ cannot clearly state the characteristics of __Time__ and __Space__. My theory, the space-time bubble theory, clearly explains time and space.

As said by _Albert Einstein_, space and time works relative to each other. When space expands, time slows down and when space contracts, time passes faster. Space expands and contracts because it is made of a fabric: dark matter. According to me, space is very small without matter in it. When matter is put into the fabrics of space (dark matter), It expands and becomes bubble shaped as the concentration of matter is the same everywhere in space. Dark matter has very very minute particles with mass. We can’t feel them around because they are connected to each other with very strong tensile force making them very flexible. Their flexibility and tiny size fail us to feel the dark matter.

The gravitational pull of the celestial bodies is caused due to the stretching of the fabric in which the celestial bodies lie.as we all know that the space is expanding. Some theories say that the expansion will go on while some says that gravity will overcome the expansion and will contract again to a single energy point. But as we all know that space is expanding in an accelerating manner, there has to be an invisible energy which is being constantly created in our universe, the dark energy. It is created when matter and anti-matter collide. In our universe, the matter and antimatter constantly collide to generate dark energy, the energy which expands space.

All the universes including our universe was once concentrated in a small spacetime bubble. When the dark energy expanded the space within the bubble, the cooling of the bubble caused the dark energy to convert into matter and anti matter. After a few Planck seconds, the bubble splits into many space time bubbles, which eventually forms individual universe. As each universe has a mixture of _anti matter and matter_, they collide with each other to form __dark energy__. In every _bubble or universe_, the number of matter and antimatter varies. After the collisions, the particle which is larger in number than the other forms the stars and galaxies. Therefore every universe or bubble is either made of _matter or antimatter_. After billions of years, due to the expansion of the space by dark energy, each bubble again comes in contact with each other. At that very moment, the walls between each universe or bubble merge. This phenomenon is called coherence. When the walls break the matter and anti-matter again collide to form dark energy. At this time, when only energy and no matter is left the fabric of space, dark matter contracts to form a small space-time bubble and the cycle continues.

When an object is in motion, it doesn’t move at all. It is the space around him which moves. For example, imagine yourself to be in a cage made of strings. You certainly will not be able to move. But if your cage is put on truck and moved then you will be able to move. My theory perfectly explains why light, moving at the speed of light doesn’t experience a change in its time and space, but a car moving with speed of light does. Light, is made up of photons, particles without mass. Since light doesn’t have any mass, it doesn’t affect the space and time of itself. However, a car has mass. When it moves with speed of light, the space around itself expands and time contracts. Therefore when you move, dark energy expands space to make you move.

The time ended when the space expanded to it its max, which was when coherence took place. When space contracted into the space-time bubble, and then expanded to form other bubbles, the time started again.

> The most important thing is that the time goes not for the empty box but for each and every Bubble or Universe in the empty box.

When the spacetime bubble split, the time started. The first bubble which comes out of the space time bubble, starts its time first and the last bubble to split will start its time last. So, you can watch your past and future if you can travel to other bubbles.for this you will have to travel with more than speed of light.You cannot change the past because you may not be sure whether the bubble you are entering is made by matter or antimatter. Entering an unknown bubble can be dangerous. Even then if you maybe lucky and go to your like particle bubble, you can change the present of the bubble you have entered. You can change your future by watching your future from other bubble and making the change accordingly in our present so as to change the future. That is why it is said,

> The future is in your hands, to unravel the mysteries of our Universe.. You will have to make your bark work to 100C/o because all the secrets of the universe including us were once united in the same __Space-Time__ Bubble!
