---
title: Links
date: 2018-01-13 21:02:16 +0530
description: "I have been using these Services for my blog! Thanks to them!"
---
# Links

* [Privacy Policy](/privacy-policy/)
* [About Me](/about/)
* [Sitemap](/sitemap.xml)
* [Contact](/contact/)

## Social

* [Twitter](https://twitter.com/HorribleGeek/)
* [GitHub](https://github.com/HorribleGeek/)
* [GitLab](https://gitlab.com/HorribleGeek/)

This Blog's Repository - [GitLab Repo - Geek's World](https://gitlab.com/HorribleGeek)

## Credits

* Host : Netlify
* Generator : Hugo
* DNS : Cloudflare
* Repository : GitLab
* Text Editor : Atom
* Hugo Theme : [Black Burn](https://github.com/yoshiharuyamashita/blackburn)
* Comments CSS: [Talented Unicorn - CodePen](https://codepen.io/talentedunicorn/pen/rEqsf "Talented Unicorn - CodePen")
* Images: Pixabay or Pexels
