---
title: "About"
type: "page"
date: "2017-12-31"
description: "About Me and this Blog!"
---
Hey there!

My name is Abhinav S Thakur, I am from India. I love learning new things and share the same over here. I am currently studying in X Grade and in a month or two I'll be studying in XI Grade! Well, I'm 16 now and will turn 17 on July 11 2018.

## About this Blog

I started this off with no idea.. I had no clue about how far this will take me. After buying this domain in September 2017, I used it for 2 months as a E-Mail Service.

Then I started this blog in late December and from then I have learnt a lot. Over here I write about the stuff that I do or simply share the knowledge that I have with you all!

Before starting this I have worked with Web Hosting, cPanel, WHM, WordPress, Blogger, Joomla.. and many other CMS. You can ask me questions on these topics, I'll try to do my best to help you out!

After a few weeks on Blogger, I moved to Hugo - Static Site Generator! My experience with _Hugo_ has been good overall. Switching to a Static Site was a good idea because I've learnt a lot since then.

HTML, CSS and JS were known to me but I didn't work on them because I never got a chance to work on them. Also, I didn't understand the concept of git.. Hugo was the reason I've learnt about git version control!

I also have a [links](/links/) page which is credits page actually. There you will find links to almost everything used to make this Blog Awesome!
