---
title: "Contact"
type: "page"
date: "2018-01-10"
description: "Drop me a message, I'll try to reply asap."
---

Use this form to drop me a message or else you can also mail me at `Abhinav@coregossip.com` for a quick talk. E-Mail is collected only for replying you back with a proper response. If you don't want to disclose your E-mail then feel free to use any random address. This Form is powered by Netlify, all I get to see is the response sheet. Nothing is logged!
<div id="contact_form">
<form class="pure-form" name="Contact" netlify>
	 <fieldset class="pure-group">
		<input type="text" class="pure-input-1-3" name="name" placeholder="Your Name">
		<input type="email" class="pure-input-1-3" name="email" placeholder="Your Email">
	</fieldset>
	<textarea name="message" class="pure-input-1-2" placeholder="Message" rows="5"></textarea>
	<p>
		<button type="submit" class="pure-button pure-input-1-2 pure-button-primary" required>Send</button>
	</p>
</form>
</div>
