---
title: "Thank You for Commenting!"
type: "page"
date: "2017-12-31"
slug: /thank-you/
---
You Comment has been recorded.

Please be patient because it may take some time to show up on the comments page. Click here to <button class="pure-button" onclick="goBack()">Go Back!</button>
<script>
function goBack() {
    window.history.back();
}
</script>
