---
type: "page"
title: "Privacy Policy"
date: 2018-01-27
description: "Your Privacy is important for me, unlike the old page this will be straight and simple"
aliases:
    - /privacy
---
## Adverts
I may occasionally enable Ads by Google on this site, you can use AdBlocker though!

> I'm looking for a Advertiser. If you are interested then contact me!

## Comments
Comments you submit on this blog only collects your E-Mail and IP. However, I have nothing to do with your E-Mail Address and IP.. all the records are deleted as soon as I see the comment!

> Note: IP collection is automatic and is not in my control!

## E-Mail
Your E-Mail is collected when you fill the contact form/comment. No other method is used for E-Mail collection.

> Your E-Mail is not sold to third party clients, it can only be accessed by authorised users (me and only me). Your E-Mail will not be used for any other purpose.

## Analytics
Like Adverts I may enable Analytics occasionally!

> I am not using Google Analytics, instead I use Clicky and Momently!

## Cookies
Ah! Everyone does this but this site is Simple HTML. So.. what cookies shall we keep..?

External Services that we use may use cookies!

## Cache
Yes! All the image files, javascript and css files are cached in your Browser. Nothing else is kept as cache, all these cache files are stored for saving your bandwidth and faster site load!

## Personal Data
No personal data is collected expect for E-Mail (if you drop your E-Mail in contact form/comment).

## Sharing of Data
No Data is shared with anyone else expect the service we use. Google's Services used on this blog, which are enabled very rarely for a short amount of time may use your data to provide you with personalised content!

## Conclusion
Your Privacy is my concern, surf through this site without any issues! Use AdBlock if you don't want Google to track you because as said I may occasionally enable Ads on this site.

> Last Updated: 28th January 2018

> This Page can be updated anytime without informing anyone for any stupid reason!
